import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import PrimaryHeader from "../ui/PrimaryHeader";
import { withFirebase } from "../firebase";
import ContactCheckoutPage from "../pages/ContactCheckoutPage";
import TitleCase from "../utils/Utils";

// Sub Layouts
import UserSubLayout from "./UserSubLayout";
import ProductSubLayout from "./ProductSubLayout";
import { StripeProvider } from "react-stripe-elements";

import HelpPage from "../pages/HelpPage";
import GiftsSent from "../pages/GiftsSent";
import GiftsReceived from "../pages/GiftsReceived";
import WishListPage from "../pages/WishListPage";
import FavoritesPage from "../pages/FavoritesPage";
import { withAuthorization } from "../session";
import FriendsContactsPage from "../pages/FriendsContactsPage";
import ProductPage from "../pages/ProductPage";
import Footer from "../components/Footer";

const PrimaryLayout = ({ match, history }) => (
  <StripeProvider apiKey={process.env.REACT_APP_STRIPE_TOKEN}>
    <PrimaryLayoutPage match={match} history={history} />
  </StripeProvider>
);

class PrimaryLayoutPageBase extends React.Component {
  constructor(props) {
    super(props);

    this.match = this.props.match;
    this.history = this.props.history;

    this.state = {
      product: {},
      user: {},
      recipient: {},
      step: 0
    };
  }

  componentDidMount() {
    if (this.props.firebase.auth.currentUser == null) {
      this.listener = this.props.firebase.auth.onAuthStateChanged(authUser => {
        this.getUser(this.props.firebase.auth.currentUser.uid);
      });
    } else {
      this.getUser(this.props.firebase.auth.currentUser.uid);
    }
  }

  getUser = userid => {
    let user = this.props.firebase.user(userid);
    user.get().then(doc => {
      if (doc.exists) {
        let objUser = doc.data();
        objUser.id = userid;
        objUser.providerid = this.props.firebase.auth.currentUser.providerData[0].providerId;
        objUser.displayName = TitleCase(
          this.props.firebase.auth.currentUser.displayName
        );
        objUser.password = "";
        objUser.passwordrepeat = "";
        objUser.photoURL = this.props.firebase.auth.currentUser.photoURL;
        this.setState({ user: objUser });
      } else {
        let objUser = {
          id: userid,
          email: this.props.firebase.auth.currentUser.email,
          firstname: TitleCase(
            this.props.firebase.auth.currentUser.displayName
          ),
          lastname: "",
          phonenumber: "",
          birthday: "",
          gender: "",
          providerid: this.props.firebase.auth.currentUser.providerData[0]
            .providerId,
          password: "",
          passwordrepeat: "",
          photoURL: this.props.firebase.auth.currentUser.photoURL
        };
        this.setState({ user: objUser });
      }
    });
  };

  componentWillUnmount() {
    if (this.listener) {
      this.listener();
    }
  }

  render() {
    return (
      <div>
        <PrimaryHeader user={this.state.user} />
        <main>
          <Switch>
            <Route
              path={`${this.match.path}/home`}
              component={ProductSubLayout}
            />
            <Route
              path={`${this.match.path}/users`}
              component={UserSubLayout}
            />
            <Route path={`${this.match.path}/help`} component={HelpPage} />
            <Route
              path={`${this.match.path}/giftssent`}
              component={GiftsSent}
            />
            <Route
              path={`${this.match.path}/giftsreceived`}
              component={GiftsReceived}
            />
            <Route
              path={`${this.match.path}/contact-checkout/:productId`}
              render={() => (
                <ContactCheckoutPage
                  user={this.state.user}
                  step={this.state.step}
                  history={this.history}
                />
              )}
            />
            <Route
              path={`${this.match.path}/wishlist`}
              exact
              component={WishListPage}
            />
            <Route
              path={`${this.match.path}/wishlist/detail/:productId`}
              component={ProductPage}
            />
            <Route
              path={`${this.match.path}/favorites`}
              exact
              component={FavoritesPage}
            />
            <Route
              path={`${this.match.path}/favorites/detail/:productId`}
              component={ProductPage}
            />
            <Route
              path={`${this.match.path}/friendscontacts`}
              exact
              component={FriendsContactsPage}
            />
            <Redirect to={`${this.match.url}/home`} />
          </Switch>
        </main>
        <Footer />
      </div>
    );
  }
}

const condition = authUser => !!authUser;

const PrimaryLayoutPage = withFirebase(
  withAuthorization(condition)(PrimaryLayoutPageBase)
);

export default PrimaryLayout;
