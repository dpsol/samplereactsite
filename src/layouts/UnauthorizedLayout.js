import React from "react";
import PropTypes from "prop-types";
import "../scss/main.scss";
import "../scss/landing.scss";
import { Grid, Row, Col } from "react-flexbox-grid";
import DottedMap from "../assets/landing/dotted-map.png";
import Android from "../assets/landing/android.png";
import Apple from "../assets/landing/apple.png";
import Email from "../assets/landing/email.png";
import ArrowLeft from "../assets/landing/arrow-left.png";
import {
  BorderButton,
  PrimaryButton,
} from "../components/Button";
import ResetPassword from "../components/ResetPassword";
import FacebookLogin from "../components/FacebookLogin";
import GoogleLogin from "../components/GoogleLogin";
import EmailSignUpPage from "../components/EmailSignup";
import EmailSignInPage from "../components/EmailLogin";
import Footer from "../components/Footer";

class UnauthorizedLayout extends React.Component {
  state = {
    selectedLoginMethod: "login",
    isBackEnabled: false
  };

  _renderLoginSwitch = param => {
    switch (param) {
      case "login": {
        return (
          <React.Fragment>
            <BorderButton
              fullWidth
              onClick={() =>
                this.setState({
                  selectedLoginMethod: "signupWithEmail",
                  previousLoginMethod: "login",
                  isBackEnabled: true
                })
              }
            >
              <img
                classsName="btn-icon"
                src={Email}
                alt="Email icon"
                style={{ width: "20px", marginRight: "10px" }}
              />
              Sign up with email
            </BorderButton>
            <FacebookLogin />
            <GoogleLogin />
            <div className="divider">
              <h4>
                <span>Or</span>
              </h4>
            </div>
            <PrimaryButton
              fullWidth
              onClick={() =>
                this.setState({
                  selectedLoginMethod: "loginWithEmail",
                  previousLoginMethod: "login",
                  isBackEnabled: true
                })
              }
            >
              Login
            </PrimaryButton>
            <p>
              By clicking 'Sign up with Facebook' or 'Sign up with Google' you
              agree to the Terms of Use and Privacy Policy
            </p>
          </React.Fragment>
        );
      }

      case "signupWithEmail":
        return <EmailSignUpPage />;

      case "loginWithEmail":
        return (
          <React.Fragment>
            <EmailSignInPage />
            <div className="divider">
              <button
                class="link"
                onClick={() =>
                  this.setState({
                    selectedLoginMethod: "resetPassword",
                    previousLoginMethod: "loginWithEmail"
                  })
                }
              >
                Forgot password?
              </button>
              <h4>
                <span>Or</span>
              </h4>
            </div>
            <FacebookLogin />
            <GoogleLogin />
            <p>
              By clicking 'Sign up with Facebook' or 'Sign up with Google' you
              agree to the Terms of Use and Privacy Policy
            </p>
          </React.Fragment>
        );

      case "resetPassword":
        return <ResetPassword />;
      default:
        return (
          <div>
            <BorderButton fullWidth>
              <img
                classsName="btn-icon"
                src={Email}
                alt="Email icon"
                style={{ width: "20px", marginRight: "10px" }}
              />
              Sign up with email
            </BorderButton>
            <FacebookLogin />
            <GoogleLogin />
            <div className="divider">
              <h4>
                <span>Or</span>
              </h4>
            </div>
            <PrimaryButton fullWidth>Login</PrimaryButton>
            <p>
              By clicking 'Sign up with Facebook' or 'Sign up with Google' you
              agree to the Terms of Use and Privacy Policy
            </p>
          </div>
        );
    }
  };

  render() {
    return (
      <div id="landing">
        <Grid fluid className="landing-content">
          <Row>
            <Col xs={12}>
              <Row center="xs">
                <Col xs={6}>
                  <img
                    className="logo"
                    src="/images/-logo.png"
                    align="center"
                    alt="Logo"
                  />
                  <h1></h1>
                </Col>
              </Row>
            </Col>
          </Row>

          <div className="login-wrap">
            <Row>
              <Col xs={12} mdOffset={1} md={5} lg={5}>
                <h2>WORLDWIDE GIFTING MADE EASY</h2>
                <img
                  className="dotted-map"
                  src={DottedMap}
                  align="center"
                  alt="Dotted map"
                />
                <BorderButton
                  as="a"
                  href=""
                  target="_blank"
                  style={{
                    width: "268px",
                    display: "block",
                    margin: "0 auto",
                    textAlign: "center",
                    lineHeight: "40px",
                    marginBottom: "40px"
                  }}
                >
                  LEARN MORE
                </BorderButton>
              </Col>
              <Col id="login-flow" xs={12} mdOffset={1} md={4} lg={4}>
                <div style={{ position: "relative" }}>
                  {this.state.isBackEnabled ? (
                    <button
                      style={{
                        height: "40px",
                        marginBottom: "0",
                        verticalAlign: "middle",
                        position: "absolute",
                        padding: "0",
                        left: "0"
                      }}
                      onClick={() => {
                        this.setState({
                          selectedLoginMethod: this.state.previousLoginMethod
                        });
                        if (this.state.previousLoginMethod === "login") {
                          this.setState({ isBackEnabled: false });
                        }
                        if (
                          this.state.previousLoginMethod === "loginWithEmail"
                        ) {
                          this.setState({
                            previousLoginMethod: "login"
                          });
                        }
                      }}
                    >
                      <img
                        src={ArrowLeft}
                        alt="Arrow left"
                        style={{ width: "25px" }}
                      />
                    </button>
                  ) : (
                    <div
                      style={{
                        height: "40px",
                        width: "40px",
                        display: "inline-block",
                        verticalAlign: "middle",
                        position: "absolute",
                        left: "0"
                      }}
                    />
                  )}

                  <h3>Sign up and start</h3>
                </div>

                {this._renderLoginSwitch(this.state.selectedLoginMethod)}
              </Col>
            </Row>
          </div>

          <div className="download">
            <Row>
              <Col xs={12}>
                <div className="download-btn">
                  <PrimaryButton
                    as="a"
                    href="/"
                    target="_blank"
                    style={{ fontFamily: "Lato" }}
                  >
                    <img src={Apple} alt="Apple icon" /> FOR IPHONE
                  </PrimaryButton>
                </div>
                <div className="download-btn">
                  <PrimaryButton
                    as="a"
                    href="https://play.google.com/store"
                    target="_blank"
                    style={{ fontFamily: "Lato" }}
                  >
                    <img src={Android} alt="Android icon" />
                    FOR ANDROID
                  </PrimaryButton>
                </div>
              </Col>
            </Row>
          </div>
        </Grid>
        <Footer />
      </div>
    );
  }
}

UnauthorizedLayout.propTypes = {
  classes: PropTypes.object.isRequired
};

export default UnauthorizedLayout;
