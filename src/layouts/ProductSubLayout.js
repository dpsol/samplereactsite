import React from "react";
import { Switch, Route } from "react-router-dom";

// Sub Layouts
import BrowseProductPage from '../pages/BrowseProductsPage';

const ProductSubLayout = ({ match }) => (
  <div>
    <Switch>
      <Route path={match.path} exact component={BrowseProductPage} />
      <Route path={`${match.path}/detail/:productId`} component={BrowseProductPage} />
    </Switch>
  </div>
);

export default ProductSubLayout;
