import React from "react";
import { Switch, Route, NavLink } from "react-router-dom";
import UserNav from "../ui/UserNav";

// Sub Layouts
import UserProfilePage from "../pages/UserProfilePage";
import UserSettingsPage from "../pages/UserSettingsPage";

const UserSubLayout = ({ match }) => (
  <div className="user-sub-layout">
    <div className="primary-content">
      <Switch>
        <Route path={`${match.path}/settings/`} component={UserSettingsPage} />
        <Route path={`${match.path}/:userId`} component={UserProfilePage} />
      </Switch>
    </div>
  </div>
);

export default UserSubLayout;
