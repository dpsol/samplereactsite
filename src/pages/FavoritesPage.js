import React, { Component } from "react";
import { withFirebase } from "../firebase";
import { Typography, Grid } from "@material-ui/core";
import CardList from "../components/ProductCards";
import LeftMenu from "../ui/LeftMenu";
import Message from "../ui/Message";

class FavoritesPageBase extends Component {
  constructor(props) {
    super(props);

    this.match = this.props.match;

    this.state = {
      products: [],
      wishlist: [],
      open: false,
      message: ""
    };
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    this.loadFavorites();
  }

  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    this.setState({ open: false });
  };

  handleClick(product) {
    // Delete from favorites
    let user = this.props.firebase.user(
      this.props.firebase.auth.currentUser.uid
    );
    user
      .get()
      .then(doc => {
        if (doc.exists) {
          let { favorites } = doc.data();
          if (favorites) {
            const index = favorites.indexOf(product.key);
            if (index > -1) {
              favorites.splice(index, 1);
              user
                .update({
                  favorites
                })
                .then(() => {
                  this.loadFavorites();
                });
              this.setState({
                message: "Product removed from your favorites list!",
                open: true
              });
            }
          }
        }
      })
      .catch(function(error) {
        console.error("Error removing product from favorites list: ", error);
      });
  }

  handleWishlistClick = product => {
    // Add or remove to Wish list
    let wishlist = this.state.wishlist;
    if (product.onWishlist) {
      //Remove from wishlist
      const index = wishlist.indexOf(product.key);
      if (index > -1) {
        wishlist.splice(index, 1);
        this.setState({
          wishlist,
          message: "Product removed from your wish list!",
          open: true
        });
      }
    } else {
      // Add to favorites
      wishlist.push(product.key);
      this.setState({
        wishlist,
        message: "Product added to your wish list!",
        open: true
      });
    }

    let user = this.props.firebase.user(
      this.props.firebase.auth.currentUser.uid
    );
    user.get().then(doc => {
      if (doc.exists) {
        user.update({
          wishlist
        });
      }
    });

    product.onWishlist = !product.onWishlist;
  };

  toggleProduct = product => {};

  loadFavorites = () => {
    this.setState({ products: [] });
    let user = this.props.firebase.user(
      this.props.firebase.auth.currentUser.uid
    );
    user
      .get()
      .then(doc => {
        if (doc.exists) {
          let { favorites, wishlist } = doc.data();

          if (favorites == null) {
            favorites = [];
          }
          if (wishlist == null) {
            wishlist = [];
          }

          favorites.forEach(productid => {
            this.props.firebase
              .variant(productid)
              .get()
              .then(docvar => {
                if (docvar.exists) {
                  // Verifies if is on wishlist
                  const onWishlist = wishlist.indexOf(docvar.id) > -1;

                  const {
                    assets,
                    attrs,
                    desc,
                    name,
                    price,
                    pricedisc,
                    color,
                    sku
                  } = docvar.data();
                  // Get the first variant's image
                  const imageUrl =
                    assets.images != null ? assets.images[0].url : "";
                  // Get Brand from attrs
                  const brandElement = attrs.find(
                    item => item.name.toLowerCase() === "brand"
                  );
                  const Brand = brandElement.value;
                  // Get desc (on english)
                  const descElement = desc.find(
                    item => item.lan.toLowerCase() === "en"
                  );
                  const Description = descElement.val;
                  const ModelName = name;
                  const PriceDisc = pricedisc != null ? pricedisc : 0;
                  const Price = PriceDisc == 0 ? price : PriceDisc;
                  let item = {
                    key: docvar.id,
                    Brand,
                    Description,
                    Price,
                    ModelName,
                    imageUrl,
                    color,
                    sku,
                    onWishlist,
                    onFavorites: true
                  };
                  this.setState(prevState => {
                    prevState.products.push(item);
                  });
                }
                this.forceUpdate();
              })
              .catch(function(error) {
                console.error("Error getting wishlist: ", error);
              });
          });
        }
      })
      .catch(function(error) {
        console.error("Error getting user's data", error);
      });
  };
  render() {
    return (
      <Grid
        container
        style={{ paddingBottom: 15, minHeight: "calc(100vh - 76px)" }}
      >
        <LeftMenu opSelected={6} />
        <Grid item xs={9} style={{ padding: 30 }}>
          <h6
            style={{
              fontFamily: "Lato",
              fontSize: 22,
              fontWeight: 300,
              marginBottom: 40
            }}
          >
            Favorites
          </h6>
          {/* Body */}
          <CardList
            products={this.state.products}
            match={this.match}
            locationid={this.state.products.cityid}
            handleClick={this.handleWishlistClick}
            handleFavoriteClick={this.handleClick}
            toggleProduct={this.toggleProduct}
            source="favorites"
          />

          <Message
            message={this.state.message}
            open={this.state.open}
            handleClose={this.handleClose}
            variant={"success"}
          />
        </Grid>
      </Grid>
    );
  }
}

const FavoritesPage = withFirebase(FavoritesPageBase);

export default FavoritesPage;
