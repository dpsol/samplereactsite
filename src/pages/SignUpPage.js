import React, { Component } from "react";
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";
import { login } from "../utils/xhr";
import { auth, db } from '../firebase';
import { firestore } from '../firebase/firebase';
import { Typography, Button, TextField, Grid, Icon, withStyles } from '@material-ui/core';
import { Mixpanel } from '../utils/MixpanelWrapper';

const SignUpPage = ({ history }) => <SignUpForm history={history} />;

const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value
});

const INITIAL_STATE = {
  username: '',
  userlastname: '',
  email: '',
  passwordOne: '',
  error: null,
};

const styles = theme => ({
  emailIcon: {
      marginRight: theme.spacing.unit,
  },
  arrowIcon: {
    transform: 'rotate(90deg)',
  },
  inputBox: {
      fontSize: 15,
      lineHeight: 1,
      paddingTop: 10,
      paddingBottom: 5,
    },
  formTextLabel: {
    fontSize: 14,
    lineHeight: 0.4,
  },
  formControlLabel: {
    fontSize: 12,
    lineHeight: 0,
    padding: 0,
    marginTop: 6,
    transform: 'translate(14px, 11px) scale(1)',
  },
  formControlLabel2: {
    fontSize: 12,
    lineHeight: 0,
    padding: 0,
    marginTop: 6,
    transform: 'translate(14px, 5px) scale(1)',
  },
  textFieldClass: {
    marginTop: 15,
    fontSize:10,
  },
});

class SignUpForm extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  componentDidMount() {
    Mixpanel.track('Website - Signup.Click.');
  }

  onSubmit = event => {
    Mixpanel.track('Website - Signup.User.');
    const {
      username,
      userlastname,
      email,
      passwordOne,
    } = this.state;

    const {
      history,
    } = this.props;

    // Useraccount = first part of email and unique
    let useraccount = email.split('@')[0];
    // Search username to avoid duplicates
    console.log("antes " + useraccount);
    
    firestore.collection("users").where("username", "==", useraccount)
        .get()
        .then( querySnapshot => {
            useraccount += Math.random().toString(36).slice(-5); 
            console.log("despues " + useraccount);    
          })

    auth.doCreateUserWithEmailAndPassword(email, passwordOne)
      .then(authUser => {
        // Create a user in your own accessible Firebase Database too
        // Useraccount = first part of email and unique
        let useraccount = email.split('@')[0];
        db.doCreateUser(authUser.user.uid, username, userlastname, email, useraccount)
          .then(() => {
            console.log("pasó el then del doCreateUser");
            this.setState({ ...INITIAL_STATE });
            login().then(() => { 
              Mixpanel.track('Website - Signup.Successfull.');
              history.push('/app') 
             });
             //    history.push(routes.HOME);
          })
          .catch(error => {
            Mixpanel.track('Website - Signup.Failed.');
            this.setState(byPropKey('error', error));
          });

          this.setState({ ...INITIAL_STATE });
          //history.push(routes.HOMESTORE);
      })
      .catch(error => {
        Mixpanel.track('Website - Signup.Failed.');
        this.setState(byPropKey('error', error));
      });

    event.preventDefault();
  };

  render() {
    const { classes } = this.props;

    const {
        username,
        userlastname,
        email,
        passwordOne,
        error,
      } = this.state;
      
    const isInvalid =
        passwordOne === '' ||
        email === '' ||
        username === '' ||
        userlastname === '';

    return (
      <Grid container
            direction="column"
            alignItems="center"
            justify="center">
        <Grid item xs={12}>
          <div style={{ paddingTop: '13px'}}>
          <Typography variant="h6" align="center" color="secondary">
              <Link to={"/auth"}>
                  <Icon style={{ marginRight:10, verticalAlign:"middle", color:'grey' }}>keyboard_backspace</Icon>
              </Link>
              Sign up and start
            </Typography>
          </div>
          <form onSubmit={this.onSubmit}>
            <div>
            <TextField
                id="firstname"
                label="FIRST NAME"
                variant="outlined"
                fullWidth
                value={username}
                onChange={event => this.setState(byPropKey('username', event.target.value))}
                inputProps={{ className: classes.inputBox}}
                InputLabelProps={{ classes: {
                    root: classes.formControlLabel,
                    focused: classes.formTextLabel,
                  }
                }}
                style={{marginTop: 5}}
            />
          </div>
          <div>
            <TextField
                id="lastname"
                label="LAST NAME"
                variant="outlined"
                fullWidth
                value={userlastname}
                onChange={event => 
                  this.setState(byPropKey('userlastname', event.target.value))}
                inputProps={{ className: classes.inputBox}}
                InputLabelProps={{ classes: {
                    root: classes.formControlLabel,
                    focused: classes.formTextLabel,
                  }
                }}
                style={{marginTop: 8}}
            />
          </div>
          <div>
              <TextField 
                id="email"
                label="EMAIL"
                variant="outlined"
                fullWidth
                type="email"
                value={email}
                onChange={event => this.setState(byPropKey('email', event.target.value))}
                inputProps={{ className: classes.inputBox}}
                InputLabelProps={{ classes: {
                    root: classes.formControlLabel,
                    focused: classes.formTextLabel,
                  }
                }}
                style={{marginTop: 8}}
            />
          </div>
          <div>
              <TextField 
                id="passwordone"
                label="PASSWORD"
                variant="outlined"
                fullWidth
                autoComplete="current-password"
                type="password"
                value={passwordOne}
                onChange={event => this.setState(byPropKey('passwordOne', event.target.value))}
                inputProps={{ className: classes.inputBox}}
                InputLabelProps={{ classes: {
                    root: classes.formControlLabel,
                    focused: classes.formTextLabel,
                  }
                }}
                style={{marginTop: 8}}
            />
          </div>
          <div>
              <Button variant="contained" color="primary" size="small" fullWidth
                disabled={isInvalid} type="submit" style={{marginTop: 8}}>
                SIGN UP
              </Button>
          </div>
          {error && <p>{error.message}</p>}
        </form>
        </Grid>
      </Grid>
    );
  }
}

class SignUpLink extends React.Component {
  componentDidMount() {
    Mixpanel.trackLink("#signupButton", "Website - Signup.Click.")
  }

  render() {
    return (
      <Link id="signupButton" to={"/auth/signup"} style={{ textDecoration: 'none' }}>
        <Button variant="outlined" color="primary" size="small" fullWidth>
            <Icon fontSize='small' style={{ marginRight: 8 }}>email</Icon>
            SIGN UP WITH EMAIL
        </Button>
      </Link>
    )
}};

SignUpForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

SignUpForm = withStyles(styles)(SignUpForm);

export default SignUpPage;
export { SignUpLink };
