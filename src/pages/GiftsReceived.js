import React, { Component } from "react";
import { withFirebase } from "../firebase";

import { Grid, Typography, Hidden } from "@material-ui/core";
import {
  Card,
  CardActionArea,
  CardContent,
  CardMedia
} from "@material-ui/core";
import LeftMenu from "../ui/LeftMenu";

class GiftsReceivedBase extends Component {
  constructor(props) {
    super(props);

    this.match = this.props.match;

    this.state = {
      products: [],
      count: 0
    };
  }

  componentDidMount() {
    const userEmail = this.props.firebase.auth.currentUser.email;

    this.props.firebase
      .gifts()
      .where("gift_receiver.receiver_email", "==", userEmail)
      .onSnapshot(querySnapshot => {
        this.setState({ count: querySnapshot.size });
        querySnapshot.forEach(docGift => {
          let onWishlist = false;
          let onFavorites = false;

          const { date_bought, gift_info, gift_giver } = docGift.data();
          const date = new Date(date_bought);
          const dateOptions = {
            year: "numeric",
            month: "long",
            day: "numeric"
          };

          this.props.firebase
            .variant(gift_info.id)
            .get()
            .then(docvar => {
              if (docvar.exists) {
                // Verifies if is on wishlist or favorites
                this.props.firebase
                  .user(this.props.firebase.auth.currentUser.uid)
                  .get()
                  .then(user => {
                    if (user.exists) {
                      const { favorites, wishlist } = user.data();
                      onWishlist = wishlist.indexOf(docvar.id) > -1;
                      onFavorites = favorites.indexOf(docvar.id) > -1;
                    }
                  });
              }
            });

          let item = {
            key: docGift.id,
            ModelName: gift_info.product,
            date: date.toLocaleDateString("en-US", dateOptions),
            images: gift_info.images,
            giver: gift_giver.giver_name,
            onWishlist,
            onFavorites
            //city,
          };
          this.setState(prevState => {
            prevState.products.push(item);
          });
        });
        this.forceUpdate();
      });
  }

  render() {
    return (
      <Grid
        container
        xs={12}
        style={{ paddingBottom: 15, minHeight: "calc(100vh - 76px)" }}
      >
        <LeftMenu opSelected={3} />
        <Grid item xs={9}>
          <Grid container>
            <Grid item xs={12}>
              <Grid container style={{ padding: 30 }} direction="column">
                <Grid item>
                  <Typography variant="h6" style={{ paddingBottom: 15 }}>
                    GIFTS RECEIVED
                  </Typography>
                </Grid>
                <Grid item>
                  <Typography color="secondary" variant="body2">
                    {this.state.count} GIFTS
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12}>
              <Grid
                justify="center"
                container
                cellHeight={480}
                spacing={16}
                style={{ paddingBottom: 15, width: "100%" }}
              >
                {this.state.products.map(product => (
                  <Grid item key={product.key}>
                    {/* Card */}
                    <Card className={classes.card}>
                      <CardActionArea style={{ maxWidth: 275 }}>
                        {product.images != null && (
                          <CardMedia
                            component="img"
                            alt={product.ModelName}
                            className={classes.media}
                            image={product.images[0].url}
                            title={product.ModelName}
                            style={{ width: 275 }}
                          />
                        )}
                        <CardContent>
                          <Typography
                            style={{ width: 225 }}
                            color="secondary"
                            align="center"
                            gutterBottom
                            variant="subtitle2"
                            noWrap={true}
                          >
                            {product.ModelName}
                          </Typography>
                          <Typography
                            align="center"
                            color="secondary"
                            variant="body2"
                          >
                            Sent BY: {product.giver}
                          </Typography>
                          <Typography
                            align="center"
                            color="secondary"
                            variant="caption"
                          >
                            {product.date}
                          </Typography>
                        </CardContent>
                      </CardActionArea>
                    </Card>
                  </Grid>
                ))}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

const GiftsReceived = withFirebase(GiftsReceivedBase);
export default GiftsReceived;
