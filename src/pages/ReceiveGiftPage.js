import React from "react";
// import { db } from '../firebase/firebase';
// import { storage } from '../firebase/firebase';
import { PrimaryButton } from "../components/Button";
import { withFirebase } from "../firebase";

import {
  Typography,
  Paper,
  GridList,
  GridListTile,
  Grid,
  Button,
  TextField,
  Icon,
  Divider,
  Avatar,
  CircularProgress,
  Card,
  CardContent,
  CardMedia
} from "@material-ui/core";
//import { AppBar, Toolbar } from '@material-ui/core';

import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    margin: 20
  },
  formControl: {
    minWidth: 120,
    marginLeft: 10
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  },
  gridItem: {
    margin: `${theme.spacing.unit}px auto`,
    padding: theme.spacing.unit * 2,
    backgroundColor: "yellow"
  }
});

class ReceiveGiftBase extends React.Component {
  constructor(props) {
    super(props);

    this.match = this.props.match;

    this.state = {
      gift: {}
    };
  }

  componentDidMount() {
    this.props.firebase.gift(this.match.params.giftId).onSnapshot(doc => {
      if (doc.exists) {
        const { gift_giver, gift_info, gift_receiver } = doc.data();

        const gift = {
          id: this.match.params.giftId,
          giver: gift_giver,
          info: gift_info,
          receiver: gift_receiver
        };

        this.setState({
          gift
        });
      }
    });

    const gift = this.props.firebase.gift(this.match.params.giftId);
    if (gift != null) {
      gift.update({
        date_opened: new Date()
      });
    }
  }

  render() {
    const { classes } = this.props;
    if (this.state.gift.info == null) {
      return (
        <Grid
          container
          direction="column"
          justify="center"
          alignItems="center"
          style={{ minHeight: "100vh" }}
        >
          <Grid item xs={3}>
            <CircularProgress color="primary" />
          </Grid>
        </Grid>
      );
    }
    const occasion = this.state.gift.info.gift_occasion;
    const message = this.state.gift.info.gift_message;
    const video = this.state.gift.info.gift_video;
    const gif = this.state.gift.info.gift_gif;
    const name = this.state.gift.receiver.receiver_name;
    const giver = this.state.gift.giver.giver_name;
    const giftId = this.state.gift.id;
    return (
      <div className="container">
        <div className="row">
          <div className="col-xs-12 col-md-10 offset-md-1">
            <h3
              style={{
                backgroundColor: "#F3F3F3",
                fontSize: 20,
                fontWeight: 300,
                lineHeight: "70px",
                textAlign: "center",
                marginBottom: 36
              }}
            >
              Happy {occasion} {name}
            </h3>
          </div>
          <div className="col-xs-12 col-md-8 offset-md-2 ">
            {gif != "" && gif != null && (
              <img
                width="100%"
                src={gif}
                alt="giphy"
                style={{ marginBottom: 36 }}
              />
            )}
            {video != "" && video != null && (
              <div>
                <video
                  src={video}
                  controls
                  style={{
                    objectFit: "scale-down",
                    height: "calc(100vh - 140px)",
                    width: "100%",
                    marginBottom: 36
                  }}
                >
                  <source id="srcVideoMp4" src={video} type="video/mp4" />
                  <source id="srcVideoOgg" src={video} type="video/ogg" />
                  Your browser does not support HTML5 video.
                </video>
              </div>
            )}
          </div>
          <div className="col-xs-12 col-md-8 offset-md-2 ">
            <div style={{ marginLeft: 20 }}>
              <h5
                style={{
                  fontWeight: 300,
                  fontSize: 20,
                  marginBottom: 16
                }}
              >
                Message:
              </h5>
              <p
                style={{
                  fontWeight: 300,
                  fontSize: 16,
                  color: "rgba(0,0,0,0.4)",
                  marginBottom: 14
                }}
              >
                {message}
              </p>
              <p
                style={{
                  fontWeight: 300,
                  fontSize: 16,
                  marginBottom: 46,
                  color: "rgba(0,0,0,0.4)"
                }}
              >
                From: {giver}
              </p>
            </div>
            <Link to={`/auth/gift/detail/${giftId}`}>
              <PrimaryButton
                fullWidth
                style={{
                  paddingRight: 60,
                  paddingLeft: 60,
                  lineHeight: "48px",
                  marginBottom: 140
                }}
              >
                Open Gift
              </PrimaryButton>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

ReceiveGiftBase.propTypes = {
  classes: PropTypes.object.isRequired
};

const ReceiveGift = withFirebase(ReceiveGiftBase);

export default withStyles(styles)(ReceiveGift);
