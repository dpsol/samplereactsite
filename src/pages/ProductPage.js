/*
To be deprecated, should use: src/components/ProductDetail
*/
import React, { Component } from "react";
import { withFirebase } from "../firebase";
import { Link } from "react-router-dom";
import styled, { css } from "styled-components";
import { PrimaryButton } from "../components/Button";
import { Typography, Grid, Card, CardContent } from "@material-ui/core";
import { Mixpanel } from "../utils/MixpanelWrapper";
import CardList from "../components/ProductCards";
import Message from "../ui/Message";

const ProductContainer = styled.div`
  align-items: center;
  display: flex;
  justify-content: center;
  flex-direction: row;
  @media (min-width: 768px) and (max-width: 1024px) {
    flex-direction: column;
  }
`;

const ProductPage = ({ match }) => <ShowProductPage match={match} />;

class ShowProductPageBase extends Component {
  constructor(props) {
    super(props);
    this.match = this.props.match;

    this.state = {
      product: {},
      products: [],
      city: { id: "", name: "" },
      wishlist: [],
      openMessage: false,
      message: ""
    };
  }

  handleCloseMessage = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    this.setState({ openMessage: false });
  };

  GetUserWishFav = () => {
    // Verifies if is on wishlist or favorites
    this.props.firebase
      .user(this.props.firebase.auth.currentUser.uid)
      .get()
      .then(user => {
        if (user.exists) {
          const { favorites, wishlist } = user.data();
          this.setState({ wishlist, favorites });
        }
      });
  };

  componentDidMount() {
    Mixpanel.track("Website - ProductSelected", {
      ProductId: this.match.params.productId
    });
    //TODO: Poner en biblioteca de funciones. Get user's Favorites and Wishlist
    const cityid = localStorage.getItem("cityid");
    this.GetUserWishFav();
    this.props.firebase
      .variant(this.match.params.productId)
      .get()
      .then(docvar => {
        if (docvar.exists) {
          const { assets, attrs, desc, name, price, pricedisc } = docvar.data();
          // Get the first variant's image
          const imageUrl = assets.images != null ? assets.images[0].url : "";
          // Get Brand from attrs
          const brandElement = attrs.find(
            item => item.name.toLowerCase() === "brand"
          );
          const Brand = brandElement.value;
          // Get Category from attrs
          const categoryElement = attrs.find(
            item => item.name.toLowerCase() === "category"
          );
          const category = categoryElement.value;
          // Get desc (on english)
          const descElement = desc.find(
            item => item.lan.toLowerCase() === "en"
          );
          const Description = descElement.val;
          const ModelName = name;
          const PriceDisc = pricedisc != null ? pricedisc : 0;
          const Price = PriceDisc == 0 ? price : PriceDisc;
          let item = {
            id: docvar.id,
            Brand,
            Description,
            Price,
            ModelName,
            imageUrl,
            category
          };

          // Get products related by category
          console.log(cityid);
          console.log(category);
          this.GetVariants(cityid, category);
          this.setState({ product: item });
        }
      });

    // Getting City
    this.props.firebase
      .city(cityid)
      .get()
      .then(doc => {
        if (doc.exists) {
          const { Name } = doc.data();
          this.setState({ city: { id: doc.id, name: Name } });
        }
      });
  }

  GetVariants(citiId, category) {
    this.props.firebase
      .locations()
      .where("cityid", "==", citiId)
      .where("status", "==", true)
      .onSnapshot(querySnapshot => {
        querySnapshot.forEach(docLocation => {
          this.props.firebase
            .locationvariants()
            .where("locationid", "==", docLocation.id)
            .onSnapshot(querySnapshot => {
              querySnapshot.forEach(doclocvar => {
                const { variantid } = doclocvar.data();

                let docVariant = this.props.firebase
                  .variant(variantid)
                  .get()
                  .then(docvar => {
                    if (docvar.exists) {
                      return docvar;
                    }
                  });
                this.GetEachVariant(docVariant, category);
              });
            });
        });
      });
  }

  GetEachVariant(variant, category) {
    variant.then(doc => {
      const {
        assets,
        attrs,
        color,
        desc,
        name,
        price,
        pricedisc,
        sku
      } = doc.data();

      // Get Category from attrs
      const categoryElement = attrs.find(
        item => item.name.toLowerCase() === "category"
      );
      const categoryProduct = categoryElement.value;

      if (
        doc.id !== this.match.params.productId &&
        categoryProduct === category
      ) {
        // Get the first variant's image
        const imageUrl = assets.images != null ? assets.images[0].url : "";

        // Get Brand from attrs
        const brandElement = attrs.find(
          item => item.name.toLowerCase() === "brand"
        );
        const Brand = brandElement.value;

        // Get desc (on english)
        const descElement = desc.find(item => item.lan.toLowerCase() === "en");
        const Description = descElement.val;

        const ModelName = name;
        const PriceDisc = pricedisc != null ? pricedisc : 0;
        const Price = PriceDisc == 0 ? price : PriceDisc;

        let item = {
          key: doc.id,
          doc, // DocumentSnapshot
          Brand,
          Description,
          Price,
          ModelName,
          imageUrl,
          color,
          sku,
          onWishlist: this.state.wishlist.indexOf(doc.id) > -1,
          onFavorites: this.state.favorites.indexOf(doc.id) > -1
        };
        this.setState(prevState => {
          prevState.products.push(item);
        });
        this.forceUpdate();
      }
    });
  }

  handleWishlist = product => {
    // Add to wishlist
    let productid;
    if (product) {
      productid = product.key;
      console.log("agregando a wishlist", productid);
    } else {
      productid = this.state.product.id;
      console.log("agregando a wishlist", productid);
    }
    let wishlist = this.state.wishlist;
    if (wishlist.indexOf(productid) === -1) {
      wishlist.push(productid);
      this.setState({
        message: "Product added to your wishlist!",
        openMessage: true
      });
    } else {
      // Remove from wishlist
      const index = wishlist.indexOf(productid);
      if (index > -1) {
        wishlist.splice(index, 1);
        this.setState({
          message: "Product removed from your wish list!",
          openMessage: true
        });
      }
    }

    let user = this.props.firebase.user(
      this.props.firebase.auth.currentUser.uid
    );
    user.get().then(doc => {
      if (doc.exists) {
        user.update({
          wishlist
        });
      }
    });

    this.setState({ wishlist });
  };

  render() {
    const classes = () => ({
      root: {
        flexGrow: 1
      }
    });
    return (
      <div style={{ margin: 50 }}>
        <h4
          style={{
            fontSize: 18,
            fontWeight: 300,
            display: "inline-block",
            marginBottom: 20
          }}
        >
          This is the gift catalog for:{" "}
          <span style={{ color: "#FC6E5B" }}>{this.state.city.name}</span>
        </h4>
        <div className={classes.root}>
          <div style={{ display: "flex" }}>
            <div
              style={{
                flex: 1,
                position: "relative",
                backgroundColor: "#FFFFFF",
                padding: 10,
                marginRight: 30,
                boxShadow: "0px 0px 11px rgba(0, 0, 0, 0.05)"
              }}
            >
              <figure style={{ margin: 0 }}>
                <img
                  src={this.state.product.imageUrl}
                  style={{ minHeight: 350, width: "100%" }}
                  alt={this.state.product.ModelName}
                />
              </figure>
            </div>
            <div
              style={{
                width: 370,
                position: "relative",
                background: "#FFFFFF",
                boxShadow: "0px 0px 11px rgba(0, 0, 0, 0.04)"
              }}
            >
              <div style={{ padding: "31px 23px 0 23px" }}>
                <div style={{ display: "flex", marginBottom: 15 }}>
                  <h3
                    style={{
                      fontFamily: "Lato",
                      fontSize: 18,
                      fontWeight: 300
                    }}
                  >
                    {this.state.product.ModelName}
                  </h3>
                  <span style={{ fontSize: 16, color: "rgba(0,0,0,0.4)" }}>
                    {new Intl.NumberFormat("en-US", {
                      style: "currency",
                      currency: "USD",
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2
                    }).format(this.state.product.Price)}
                  </span>
                </div>
                <h5
                  style={{
                    fontSize: 16,
                    fontWeight: 300,
                    marginBottom: 16,
                    color: "rgba(0,0,0,0.4)"
                  }}
                >
                  {this.state.product.Brand}
                </h5>
                {this.state.wishlist.indexOf(this.state.product.id) === -1 ? (
                  <button
                    onClick={this.handleWishlist}
                    style={{
                      color: "#fc6e5b",
                      fontSize: 14,
                      marginBottom: 30,
                      padding: 0
                    }}
                  >
                    Add to wishlist +
                  </button>
                ) : (
                  <button
                    onClick={this.handleWishlist}
                    style={{
                      color: "#fc6e5b",
                      fontSize: 14,
                      marginBottom: 30,
                      padding: 0
                    }}
                  >
                    Remove from wishlist -
                  </button>
                )}
                <p
                  style={{
                    fontSize: 14,
                    fontWeight: 300,
                    marginBottom: 86,
                    lineHeight: "22px"
                  }}
                >
                  {this.state.product.Description}
                </p>
              </div>
              <Link
                to={`/app/contact-checkout/${this.match.params.productId}`}
                style={{ textDecoration: "none" }}
              >
                <PrimaryButton
                  style={{
                    background: "#fc6e5b",
                    height: 50,
                    color: "#FFFFFF",
                    width: "100%",
                    display: "block",
                    fontSize: 16,
                    fontWeight: 400,
                    position: "absolute",
                    bottom: 0
                  }}
                >
                  Gift it +
                </PrimaryButton>
              </Link>
            </div>
          </div>
          <Typography
            color="default"
            variant="button"
            gutterBottom
            style={{
              display: "inline-block",
              paddingTop: 20,
              paddingBottom: 20
            }}
          >
            YOU MAY ALSO LIKE
          </Typography>

          <Grid
            direction="row"
            container
            alignItems="flex-start"
            justify="flex-start"
          >
            <Grid item>
              {/* Body */}
              <CardList
                products={this.state.products}
                match={this.match}
                locationid={this.state.locationid}
                classes={classes}
                handleClick={this.handleWishlist}
                handleFavoriteClick={this.handleFavoriteClick}
                source="productCatalog"
              />
            </Grid>
          </Grid>

          <Message
            message={this.state.message}
            open={this.state.openMessage}
            handleClose={this.handleCloseMessage}
            variant={"success"}
          />
        </div>
      </div>
    );
  }
}

const ShowProductPage = withFirebase(ShowProductPageBase);

export default ProductPage;
