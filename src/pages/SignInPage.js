import React, { Component } from "react";
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";
import { login } from "../utils/xhr";
import { auth } from '../firebase';
import { PasswordForgetLink } from './PasswordForgetPage';
import FacebookSignInLink from '../components/FacebookSignInLink';
import GoogleSignInLink from '../components/GoogleSignInLink';
import { Typography, Button, TextField, Grid, Icon, Divider, withStyles } from '@material-ui/core';
import { Mixpanel } from '../utils/MixpanelWrapper';

const SignInPage = ({ history }) => <SignInForm history={history} />;

const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value
});

const INITIAL_STATE = {
  email: "",
  password: "",
  error: null
};

const styles = theme => ({
  inputBox: {
      fontSize: 15,
      lineHeight: 1,
      paddingTop: 10,
      paddingBottom: 5,
    },
  formTextLabel: {
    fontSize: 14,
    lineHeight: 0.4,
  },
  formControlLabel: {
    fontSize: 12,
    lineHeight: 0,
    padding: 0,
    marginTop: 6,
    transform: 'translate(14px, 11px) scale(1)',
  },
  formControlLabel2: {
    fontSize: 12,
    lineHeight: 0,
    padding: 0,
    marginTop: 6,
    transform: 'translate(14px, 5px) scale(1)',
  },
  textFieldClass: {
    marginTop: 15,
    fontSize:10,
  },
});

class SignInForm extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  onSubmit = event => {
    const { email, password } = this.state;

    const { history } = this.props;

    auth.doSignInWithEmailAndPassword(email, password)
     .then(() => {
       this.setState({ ...INITIAL_STATE });
       login().then(() => { 
        Mixpanel.track('Website - Signin.Successfull.');
        history.push('/app') 
        });
     })
     .catch(error => {
      Mixpanel.track('Website - Signin.Failed.');
      this.setState(byPropKey('error', error));
     });

    event.preventDefault();
  };

  render() {
    const { classes } = this.props;
    const { email, password, error } = this.state;

    const isInvalid = password === "" || email === "";

    return (
      <Grid container
            direction="column"
            alignItems="center"
            justify="center">
        <Grid item xs={12}>
          <div style={{ paddingTop: '13px'}}>
          <Typography variant="h6" align="center" color="secondary">
              <Link to={"/auth"}>
                <a href="#">
                  <Icon style={{ marginRight:10, verticalAlign:"middle", color:'grey' }}>keyboard_backspace</Icon>
                </a>
              </Link>
              Sign in and start
            </Typography>
          </div>
          <form onSubmit={this.onSubmit}>
          <div>
            <TextField 
              id="email"
              label="EMAIL"
              variant="outlined"
              fullWidth
              type="email"
              value={email}
              onChange={event =>
                this.setState(byPropKey("email", event.target.value))
              }
              inputProps={{ className: classes.inputBox}}
              InputLabelProps={{ classes: {
                  root: classes.formControlLabel,
                  focused: classes.formTextLabel,
                }
              }}
              style={{marginTop: 5}}
          />
          </div>
          <div>
            <TextField 
              id="password"
              label="PASSWORD"
              variant="outlined"
              fullWidth
              autoComplete="current-password"
              type="password"
              value={password}
              onChange={event =>
                this.setState(byPropKey("password", event.target.value))
              }
              inputProps={{ className: classes.inputBox}}
              InputLabelProps={{ classes: {
                  root: classes.formControlLabel,
                  focused: classes.formTextLabel,
                }
              }}
              style={{marginTop: 8  }}
          />
          </div>
          <Typography variant="body2" gutterBottom align="right" color="secondary">
            <PasswordForgetLink />
          </Typography>
          <div>
            <Button variant="contained" color="primary" size="small" fullWidth
              disabled={isInvalid} type="submit">
              LOGIN
            </Button>
          </div>
          <div>
                <Grid container alignItems="center" >
                    <Grid item xs={5} >
                      <Divider />
                    </Grid>               
                    <Grid item xs={2}>
                        <Typography color="secondary" align="center">
                            OR
                        </Typography>
                    </Grid>
                    <Grid item xs={5} >
                      <Divider />
                    </Grid>               
                </Grid>
            </div>
          <div>
            <div><FacebookSignInLink text="SIGN IN" /></div>
          </div>
          <div>
            <div><GoogleSignInLink text="SIGN IN" /></div>
          </div>
          <div>
            <Typography variant="caption" gutterBottom align="center" color="secondary">
                By clicking Sign up with facebook or sign up with Google you
                agree with the Wish Terms of Use and Privacy Policy
              </Typography>
            <div/>
          </div>
          {error && <p>{error.message}</p>}
        </form>
        </Grid>
      </Grid>
    );
  }
}

class SignInLink extends React.Component {
  componentDidMount() {
    Mixpanel.trackLink("#loginButton", "Website - Signin.Click.")
  }

  render() {
    return (
      <Link id="loginButton" to={"/auth/signin"} style={{ textDecoration: 'none' }}>
        <Button variant="contained" color="primary" size="small" fullWidth>
          LOGIN
        </Button>
      </Link>
    )
}};

SignInForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

SignInForm = withStyles(styles)(SignInForm);

export default SignInPage;
export { SignInLink };
