import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { auth, db } from "../firebase";
import {
  Button,
  Typography,
  Grid,
  Icon,
  TextField,
  withStyles
} from "@material-ui/core";
import { Mixpanel } from "../utils/MixpanelWrapper";

const PasswordForgetPage = ({ history }) => (
  <PasswordForgetForm history={history} />
);

const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value
});

const INITIAL_STATE = {
  email: "",
  error: null
};

class PasswordForgetForm extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  onSubmit = event => {
    const { email } = this.state;

    auth
      .doPasswordReset(email)
      .then(() => {
        Mixpanel.track("Website - PasswordReset.Successfull.");
        this.setState({ ...INITIAL_STATE });
      })
      .catch(error => {
        Mixpanel.track("Website - PasswordReset.Failed.");
        this.setState(byPropKey("error", error));
      });

    event.preventDefault();
  };

  render() {
    const { classes } = this.props;
    const { email, error } = this.state;

    const isInvalid = email === "";

    return (
      <Grid container direction="column" alignItems="center" justify="center">
        <Grid item xs={12}>
          <div style={{ paddingTop: "13px" }}>
            <Typography variant="h6" align="center" color="secondary">
              <Link to={"/auth"}>
                <a href="#">
                  <Icon
                    style={{
                      marginRight: 10,
                      verticalAlign: "middle",
                      color: "grey"
                    }}
                  >
                    keyboard_backspace
                  </Icon>
                </a>
              </Link>
              Forgot Password?
            </Typography>
          </div>
          <form onSubmit={this.onSubmit}>
            <div>
              <TextField
                id="outlined-with-placeholder"
                label="EMAIL"
                variant="outlined"
                fullWidth
                type="email"
                value={this.state.email}
                onChange={event =>
                  this.setState(byPropKey("email", event.target.value))
                }
                inputProps={{ className: classes.inputBox }}
                InputLabelProps={{
                  classes: {
                    root: classes.formControlLabel,
                    focused: classes.formTextLabel
                  }
                }}
                style={{ marginTop: 8 }}
              />
            </div>
            <div>
              <Button
                variant="contained"
                color="primary"
                size="small"
                fullWidth
                disabled={isInvalid}
                type="submit"
              >
                SEND
              </Button>
              {error && <p>{error.message}</p>}
            </div>
          </form>
        </Grid>
      </Grid>
    );
  }
}

class PasswordForgetLink extends Component {
  componentDidMount() {
    Mixpanel.trackLink("#forgotButton", "Website - PasswordForget.Click.");
  }

  render() {
    return (
      <Link
        id="forgotButton"
        to={"/auth/passwordforget"}
        style={{ textDecoration: "none" }}
      >
        <Button
          color="secondary"
          style={{ maxHeight: 15, marginTop: 0, marginBottom: 0, padding: 0 }}
        >
          Forgot Password?
        </Button>
      </Link>
    );
  }
}

PasswordForgetForm.propTypes = {
  classes: PropTypes.object.isRequired
};

PasswordForgetForm = withStyles(styles)(PasswordForgetForm);

export default PasswordForgetPage;
export { PasswordForgetLink };
