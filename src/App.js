import React from "react";
import { Provider } from "react-redux";
import Footer from "./components/Footer";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import CssBaseline from "@material-ui/core/CssBaseline";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import store from "./store";
import { withAuthentication } from "./session";
import "./css/bootstrap-grid.min.css";
import "./css/index.css";

// Layouts
import UnauthorizedLayout from "./layouts/UnauthorizedLayout";
import PrimaryLayout from "./layouts/PrimaryLayout";
import ReceiveLayout from "./layouts/ReceiveLayout";
import ReceiveGiftViewGift from "./pages/ReceiveGiftViewGiftPage";

// Theme's styles
const styles = {
  row: {
    display: "flex",
    justifyContent: "center"
  },
  avatar: {
    margin: 10
  },
  bigAvatar: {
    margin: 10,
    width: 60,
    height: 60,
    borderRadious: 0
  }
};

// Theme's constants
const theme = createMuiTheme({
  palette: {
    type: "light",
    primary: {
      light: "#fc6e5b",
      main: "#fc6e5b"
      // dark: auto
      // contrastText: auto
    },
    secondary: {
      light: "#a8afb6",
      main: "#9f9f9f"
      // dark will be calc
      // contrastText: '#ffcc00',
    }
    // error:
  },
  typography: {
    useNextVariants: true
  },
  spacing: {
    unit: 8
  },
  overrides: {
    MuiButton: {
      root: {
        borderRadius: 0,
        marginTop: 5,
        marginBottom: 5
      },
      containedPrimary: {
        color: "white"
      },
      textSecondary: {
        fontWeight: 400,
        fontSize: 14,
        textTransform: "none"
      }
    },
    MuiInput: {
      root: {
        margin: "dense"
      }
    }
  }
});

const App = () => (
  <React.Fragment>
    <MuiThemeProvider theme={theme}>
      <CssBaseline />
      <Provider store={store}>
        <BrowserRouter>
          <Switch>
            <Route path="/auth/gift/:giftId" exact component={ReceiveLayout} />
            <Route
              path="/auth/gift/detail/:giftId"
              component={ReceiveGiftViewGift}
            />
            <Route path="/auth" component={UnauthorizedLayout} />
            <Route path="/app" component={PrimaryLayout} />
            <Redirect to="/auth" />
          </Switch>
        </BrowserRouter>
      </Provider>
    </MuiThemeProvider>
  </React.Fragment>
);

export default withAuthentication(App);
