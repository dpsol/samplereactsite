export const SIGN_UP = '/signup';
export const SIGN_UP_GOOGLE = '/signupgoogle';
export const SIGN_IN = '/signin';
export const LANDING = '/';
export const NAVIGATION = '/';
export const HOME = '/home';
export const ACCOUNT = '/account';
export const PASSWORD_FORGET = '/pw-forget';
export const PRIMARY_LAYOUT = '/PrimaryLayout';

export const HOMESTORE = '/homestore';