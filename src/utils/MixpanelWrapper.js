import mixpanel from 'mixpanel-browser';
mixpanel.init('d3cbb4265ee4e162011f657cd839aadf');

let env_check = process.env.NODE_ENV === 'production';
//let env_check = false;
let actions = {
  identify: (id) => {
    if (env_check) mixpanel.identify(id);
  },
  alias: (id) => {
    if (env_check) mixpanel.alias(id);
  },
  track: (name, props) => {
    if (env_check) mixpanel.track(name, props);
  },
  trackLink: (linkid, event) => {
      if (env_check) mixpanel.track_links(linkid, event);
  },
  people: {
    set: (props) => {
      if (env_check) mixpanel.people.set(props);
    },
  },
};

export let Mixpanel = actions;