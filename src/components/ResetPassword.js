import React, { Component } from "react";
import PropTypes from "prop-types";
//import { auth } from "../firebase";
import { withFirebase } from "../firebase";

import { PrimaryButton } from "../components/Button";
import { Mixpanel } from "../utils/MixpanelWrapper";

class ResetPasswordBase extends Component {
  constructor(props) {
    super(props);
    this.state = { email: "", error: null };
  }

  _resetPassword = event => {
    Mixpanel.trackLink("#forgotButton", "Website - PasswordForget.Click.");

    this.props.firebase
      .doPasswordReset(this.state.email)
      .then(() => {
        Mixpanel.track("Website - PasswordReset.Successfull.");
        this.setState({ email: "", error: null });
      })
      .catch(error => {
        Mixpanel.track("Website - PasswordReset.Failed.");
        this.setState({ error: error });
      });

    event.preventDefault();
  };

  render() {
    return (
      <React.Fragment>
        <form onSubmit={this._resetPassword}>
          <input
            className="color-input"
            type="email"
            placeholder="Email"
            onChange={event => this.setState({ email: event.target.value })}
          />
          <PrimaryButton fullWidth>Send</PrimaryButton>
          {this.state.error && <p>{this.state.error.message}</p>}
        </form>
      </React.Fragment>
    );
  }
}

ResetPasswordBase.propTypes = {
  classes: PropTypes.object.isRequired
};

const ResetPassword = withFirebase(ResetPasswordBase);

export default ResetPassword;
