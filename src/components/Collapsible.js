import React from "react";
import styled, { css } from "styled-components";
import PropTypes from "prop-types";
import ArrowRight from "../assets/arrow-right.png";

const PanelHeading = styled.div`
  cursor: pointer;
  display: flex;
  align-items: center;
  line-height: 50px;
`;

const PanelCollapse = styled.div`
  overflow: hidden;
  transition: height 0.2s ease-out;
`;

const PanelBody = styled.div`
  border-bottom: solid 1px rgba(90, 91, 117, 0.2);
`;

class Collapsible extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isExpanded: false
    };
  }

  handleToggle(e) {
    e.preventDefault();
    this.setState({
      isExpanded: !this.state.isExpanded,
      height: this.refs.inner.clientHeight
    });
  }

  render() {
    const { title, children } = this.props;
    const { isExpanded, height } = this.state;
    const currentHeight = isExpanded ? height : 1;
    return (
      <div
        className={`panel ${isExpanded ? "is-expanded" : ""}`}
        style={{ borderBottom: "solid 1px rgba(90, 91, 117, 0.2)" }}
      >
        <PanelHeading onClick={e => this.handleToggle(e)}>
          <h5
            style={{
              flex: 1,
              fontFamily: "Lato",
              fontSize: 16,
              fontWeight: 300
            }}
          >
            {title}
          </h5>
          <img
            src={ArrowRight}
            alt="Arrow"
            style={{
              width: 7,
              transform: isExpanded ? "rotate(90deg)" : "none",
              transition: "transform 0.2s ease-out"
            }}
          />
        </PanelHeading>
        <PanelCollapse style={{ height: currentHeight + "px" }}>
          <PanelBody ref="inner">{children}</PanelBody>
        </PanelCollapse>
      </div>
    );
  }
}

Collapsible.propTypes = {
  title: PropTypes.string
};

export default Collapsible;
