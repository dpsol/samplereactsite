import React, { Component } from "react";
import { withRouter } from "react-router";
import { GoogleButton } from "../components/Button";
import { withFirebase } from '../firebase';
import Google from "../assets/landing/google.png";
import { Mixpanel } from "../utils/MixpanelWrapper";

const INITIAL_STATE = {
  username: "",
  userlastname: "",
  email: "",
  passwordOne: "",
  error: null
};

class GoogleLoginBase extends Component {
  constructor(props) {
    super(props);
    this.state = { ...INITIAL_STATE };
  }

  _loginWithGoogle = () => {
    Mixpanel.track("Website - SignupGoogle.Click.");
    this.props.firebase
      .doSignInWithGoogle()
      .then(result => {
        this.props.firebase
          .user(result.user.uid)
          .get()
          .then(doc => {
            if (!doc.exists) {
              // Create a user in your own accessible Firebase Database too
              let username = "";
              let usr = this.props.firebase.auth().currentUser;
              if (usr.displayName != null) {
                username = usr.displayName;
              }
              this.props.firebase
                .user(result.user.uid)
                .set({
                  firstname: username,
                  lastname: "",
                  username: username,
                  email: result.user.email
                }, {merge: true})
                .then(() => {
                  this.setState({ ...INITIAL_STATE });
                  Mixpanel.track("Website - SignupGoogle.Succesfull.");
                  localStorage.setItem("providerid", "google.com");
                  this.props.history.push("/app");
                })
                .catch(error => {
                  console.log("Error al guardar usuario");
                  Mixpanel.track("Website - SignupGoogle.Failed.");
                  alert(error.message);
                  this.setState({ error: error });
                })    
            } else {
              localStorage.setItem("providerid", "google.com");
              this.props.history.push("/app");
            }
          });
      })
      .catch(error => {
        if (error != null) {
          Mixpanel.track("Website - SignupGoogle.Failed.");
          this.setState({ error: error });
          localStorage.setItem("errorSignup", error.message);
        }
      });
  };

  render() {
    return (
      <GoogleButton
        fullWidth
        onClick={this._loginWithGoogle}
        style={{ marginBottom: "0" }}
      >
        <img
          classsName="btn-icon"
          src={Google}
          alt="Google icon"
          style={{ width: "22px", marginRight: "10px" }}
        />
        Sign up with Google
      </GoogleButton>
    );
  }
}

const GoogleLogin = withFirebase(GoogleLoginBase);

export default withRouter(GoogleLogin);
