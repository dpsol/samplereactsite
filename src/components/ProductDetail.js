import React, { Component } from "react";
import { Link } from "react-router-dom";
import { PrimaryButton } from "../components/Button";
import { Mixpanel } from "../utils/MixpanelWrapper";
import { Typography, Grid } from "@material-ui/core";
import CardList from "../components/ProductCards";
import Message from "../ui/Message";

class ProductDetail extends Component {
  constructor(props) {
    super(props);
    this.match = this.props.match;
    this.state = {
      product: {},
      products: [],
      city: { id: "", name: "" }
    };
  }

  componentDidMount() {
    Mixpanel.track("Website - ProductSelected", {
      ProductId: this.props.product.key
    });
    let alsoLike = this.props.products.filter(
      elem => elem.key !== this.props.product.key
    );
    this.setState({
      products: alsoLike,
      city: { id: this.props.cityid, name: this.props.city },
      product: this.props.product
    });
  }

  handleClick = product => {
    this.setState({ product });
    this.props.handleWishlist(product);
  };

  handleFavoriteClick = product => {
    this.setState({ product });
    this.props.handleFavoriteClick(product);
  };

  toggleProduct = product => {
    this.props.handleChangeProduct(product);
    let alsoLike = this.props.products.filter(elem => elem.key !== product.key);
    this.setState({ products: alsoLike, product });
  };

  render() {
    const classes = () => ({
      root: {
        flexGrow: 1
      }
    });
    return (
      <div>
        <div className={classes.root}>
          <div style={{ display: "flex", marginBottom: 30 }}>
            <div
              style={{
                flex: 1,
                position: "relative",
                backgroundColor: "#FFFFFF",
                padding: 10,
                marginRight: 30,
                boxShadow: "0px 0px 11px rgba(0, 0, 0, 0.05)"
              }}
            >
              <figure style={{ margin: 0 }}>
                <img
                  src={this.props.product.imageUrl}
                  style={{ minHeight: 350, width: "100%" }}
                  alt={this.props.product.ModelName}
                />
              </figure>
            </div>
            <div
              style={{
                width: 370,
                marginRight: 30,
                position: "relative",
                background: "#FFFFFF",
                boxShadow: "0px 0px 11px rgba(0, 0, 0, 0.04)"
              }}
            >
              <div style={{ padding: "31px 23px 0 23px" }}>
                <div style={{ display: "flex", marginBottom: 15 }}>
                  <h3
                    style={{
                      fontFamily: "Lato",
                      fontSize: 18,
                      fontWeight: 300
                    }}
                  >
                    {this.props.product.ModelName}
                  </h3>
                  <span style={{ fontSize: 16, color: "rgba(0,0,0,0.4)" }}>
                    {new Intl.NumberFormat("en-US", {
                      style: "currency",
                      currency: "USD",
                      minimumFractionDigits: 2,
                      maximumFractionDigits: 2
                    }).format(this.props.product.Price)}
                  </span>
                </div>
                <h5
                  style={{
                    fontSize: 16,
                    fontWeight: 300,
                    marginBottom: 16,
                    color: "rgba(0,0,0,0.4)"
                  }}
                >
                  {this.props.product.Brand}
                </h5>
                {this.props.product.onWishlist ? (
                  <button
                    onClick={() =>
                      this.props.handleWishlist(this.props.product)
                    }
                    style={{
                      color: "#fc6e5b",
                      fontSize: 14,
                      marginBottom: 30,
                      padding: 0
                    }}
                  >
                    Remove from wishlist -
                  </button>
                ) : (
                  <button
                    onClick={() =>
                      this.props.handleWishlist(this.props.product)
                    }
                    style={{
                      color: "#fc6e5b",
                      fontSize: 14,
                      marginBottom: 30,
                      padding: 0
                    }}
                  >
                    Add to wishlist +
                  </button>
                )}
                <p
                  style={{
                    fontSize: 14,
                    fontWeight: 300,
                    marginBottom: 86,
                    lineHeight: "22px"
                  }}
                >
                  {this.props.product.Description}
                </p>
              </div>
              <Link
                to={`/app/contact-checkout/${this.props.product.key}`}
                style={{ textDecoration: "none" }}
              >
                <PrimaryButton
                  style={{
                    background: "#fc6e5b",
                    height: 50,
                    color: "#FFFFFF",
                    width: "100%",
                    display: "block",
                    fontSize: 16,
                    fontWeight: 400,
                    position: "absolute",
                    bottom: 0
                  }}
                >
                  Gift it +
                </PrimaryButton>
              </Link>
            </div>
          </div>
          <Typography
            style={{
              display: "inline-block",
              paddingTop: 20,
              paddingBottom: 20,
              fontSize: 20,
              fontWeight: 300,
              marginBottom: 15
            }}
          >
            You may also like
          </Typography>

          <Grid
            direction="row"
            container
            alignItems="flex-start"
            justify="flex-start"
          >
            <Grid item>
              {/* Body */}
              <CardList
                products={this.state.products}
                match={this.match}
                locationid={this.props.locationid}
                classes={classes}
                handleClick={this.handleClick}
                handleFavoriteClick={this.handleFavoriteClick}
                source="productCatalog"
                toggleProduct={this.toggleProduct}
              />
            </Grid>
          </Grid>

          <Message
            message={this.state.message}
            open={this.state.openMessage}
            handleClose={this.handleCloseMessage}
            variant={"success"}
          />
        </div>
      </div>
    );
  }
}

//const ShowProductPage = withFirebase(ShowProductPageBase);

export default ProductDetail;
