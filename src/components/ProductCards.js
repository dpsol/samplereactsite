import React from "react";
import styled, { css } from "styled-components";
import { withFirebase } from "../firebase";

import { Typography, Grid, Button, CircularProgress } from "@material-ui/core";
import { CardActionArea, CardActions, CardContent } from "@material-ui/core";
import { Dialog, DialogContent } from "@material-ui/core";
import ShareIcon from "@material-ui/icons/Share";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
import FavoriteIcon from "@material-ui/icons/Favorite";

//import Img from "react-image";
import Shares from "./Shares";

const Card = styled.div`
  width: 270px;
  background: #ffffff;
  margin: 0 30px 30px 0;
  padding: 10px;
  box-shadow: 0 2px 19px 0 rgba(0, 0, 0, 0.04);
  transition: box-shadow 0.2s ease-in-out;
  &:hover {
    box-shadow: 0 2px 19px 0 rgba(0, 0, 0, 0.09);
  }
`;

class CardListBase extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      domain: ""
    };
  }

  componentDidMount() {
    this.props.firebase
      .param("domain")
      .get()
      .then(doc => {
        if (doc.exists) {
          this.setState({ domain: doc.data().value });
        } else {
          this.setState({ domain: "" }); //default value
        }
      })
      .catch(err => {
        console.error("Error getting domain: ", err);
        this.setState({ domain: "" }); //default value
      });
  }

  render() {
    return (
      <div style={{ display: "flex", flexWrap: "wrap" }}>
        {this.props.products.map(product => (
          <CardItem
            key={product.key}
            product={product}
            match={this.props.match}
            locationid={this.props.locationid}
            classes={this.props.classes}
            onItemClick={this.props.handleClick}
            onFavoriteClick={this.props.handleFavoriteClick}
            source={this.props.source}
            domain={this.state.domain}
            toggleProduct={this.props.toggleProduct}
          />
        ))}
      </div>
    );
  }
}

class CardItem extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false
    };
  }

  toggleModal = () => {
    this.setState(previous => ({ isOpen: !previous.isOpen }));
  };

  render() {
    const _onClick = () => {
      this.props.onItemClick(this.props.product);
    };

    const _onClickFavorite = () => {
      this.props.onFavoriteClick(this.props.product);
    };

    const styles = {
      row: {
        display: "flex",
        flexDdirection: "row",
        flexWrap: "wrap"
      },

      column: {
        flexBasis: "100%"
      },

      imgcontainer: {
        position: "relative",
        width: "100%",
        maxWidth: "275px"
      },
      buttonShare: {
        position: "absolute",
        top: -5,
        right: 0,
        maxWidth: 30,
        maxHeight: 30,
        minWidth: 30,
        minHeight: 30,
        boxShadow: "none"
      },
      buttonWish: {
        position: "absolute",
        bottom: -3,
        right: 0,
        maxWidth: 30,
        maxHeight: 30,
        minWidth: 30,
        minHeight: 30,
        fontSize: 16,
        padding: 3,
        boxShadow: "none"
      },
      iconSmall: {
        fontSize: 16
      }
    };
    return (
      <Card
        key={this.props.product.key}
        style={{ position: "relative", fontFamily: "Lato" }}
      >
        <div style={styles.imgcontainer}>
          <button
            type="button"
            onClick={() => this.props.toggleProduct(this.props.product)}
            style={{ padding: 0 }}
          >
            <figure
              style={{
                width: "100%",
                height: 250,
                maxWidth: 250,
                margin: 0,
                position: "relative"
              }}
            >
              <img
                src={this.props.product.imageUrl}
                alt={this.props.product.ModelName}
                style={{ width: "100%", maxWidth: 250, maxHeight: 250 }}
              />
            </figure>
          </button>
          <Button
            onClick={this.toggleModal}
            style={styles.buttonShare}
            variant="contained"
            color="primary"
          >
            <ShareIcon style={styles.iconSmall} />
          </Button>
          <Button
            onClick={_onClickFavorite}
            style={styles.buttonWish}
            size="small"
            variant="contained"
            color="primary"
          >
            {this.props.product.onFavorites ? (
              <FavoriteIcon style={styles.iconSmall} />
            ) : (
              <FavoriteBorderIcon style={styles.iconSmall} />
            )}
          </Button>
        </div>
        {/* </CardActionArea> */}
        <CardContent>
          <Typography
            style={{
              color: "#5A6175",
              fontSize: 16,
              fontFamily: "Lato"
            }}
            color="secondary"
            align="center"
            gutterBottom
            variant="subtitle2"
            noWrap={true}
          >
            {this.props.product.ModelName}
          </Typography>
          <Typography
            component="p"
            align="center"
            color="secondary"
            variant="body2"
            style={{ textTransform: "capitalize" }}
          >
            {this.props.product.Brand}
          </Typography>
          <Typography
            component="p"
            align="center"
            color="secondary"
            variant="caption"
          >
            {new Intl.NumberFormat("en-US", {
              style: "currency",
              currency: "USD",
              minimumFractionDigits: 2,
              maximumFractionDigits: 2
            }).format(this.props.product.Price)}
          </Typography>
        </CardContent>

        <CardActions>
          {this.props.product.onWishlist ? (
            <button
              onClick={_onClick}
              style={{
                color: "#fc6e5b",
                fontSize: 14,
                padding: 10,
                display: "block",
                width: "100%"
              }}
            >
              Remove from wishlist -
            </button>
          ) : (
            <button
              onClick={_onClick}
              style={{
                color: "#fc6e5b",
                fontSize: 14,
                padding: 10,
                display: "block",
                width: "100%"
              }}
            >
              Add to wishlist +
            </button>
          )}
        </CardActions>

        <Dialog
          open={this.state.isOpen}
          onClose={this.toggleModal}
          aria-labelledby="form-dialog-title"
        >
          <DialogContent>
            <Shares
              shareUrl={`${this.props.domain + this.props.match.path}/detail/${
                this.props.product.key
              }`}
              title={"A friend shared a product with you"}
            />
          </DialogContent>
        </Dialog>
      </Card>
    );
  }
}

const CardList = withFirebase(CardListBase);

export default CardList;
