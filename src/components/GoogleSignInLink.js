import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";
import { Button } from '@material-ui/core';
import GoogleIcon from 'mdi-react/GoogleIcon';
import { withStyles } from '@material-ui/core/styles';
import red from '@material-ui/core/colors/red';

const styles = theme => ({
  cssButton: {
    color: 'white',
    backgroundColor: '#FF5A5A',
    '&:hover': {
        backgroundColor: red[900],
    },
  },
  googleIcon: {
    marginRight: theme.spacing.unit,
  },
});
  
function GoogleSignInLink(props) {
  const { classes } = props;
  return (
    <Link to={"/auth/signupgoogle"} style={{ textDecoration: 'none' }}>
      <Button variant="contained" color="secondary" size="small" fullWidth className={classes.cssButton}>
        <GoogleIcon size="16" style={{ marginRight: 5 }} />
        {props.text} WITH GOOGLE
      </Button>
    </Link>
  )
};

GoogleSignInLink.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(GoogleSignInLink);