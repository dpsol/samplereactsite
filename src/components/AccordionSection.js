import React, { Component } from "react";
import PropTypes from "prop-types";
import ArrowRight from "../assets/arrow-right.png";

class AccordionSection extends Component {
  static propTypes = {
    children: PropTypes.instanceOf(Object).isRequired,
    isOpen: PropTypes.bool.isRequired,
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      height: 0
    };
  }

  componentDidMount() {
    const height = this.divElement.clientHeight;
    this.setState({ height });
  }

  onClick = () => {
    this.props.onClick(this.props.label);
  };

  render() {
    const {
      onClick,
      props: { isOpen, label }
    } = this;

    return (
      <div
        style={{
          width: "100%",
          lineHeight: "50px",
          padding: 0,
          borderBottom: "solid 1px rgba(90,91,117,0.2)"
        }}
      >
        <div
          onClick={onClick}
          style={{
            cursor: "pointer",
            fontFamily: "Lato",
            fontSize: 16,
            fontWeight: 300
          }}
        >
          {label}
          <div style={{ float: "right" }}>
            <img
              src={ArrowRight}
              alt="Arrow"
              style={{
                width: 7,
                transform: isOpen ? "rotate(90deg)" : "none",
                transition: "all 0.2s"
              }}
            />
          </div>
        </div>
        <div
          ref={divElement => (this.divElement = divElement)}
          style={{
            display: isOpen ? "block" : "none",
            transition: "all 0.2s linear"
          }}
        >
          <b>{this.state.height}px</b>
          {isOpen && this.props.children}
        </div>
      </div>
    );
  }
}

export default AccordionSection;
