import React, { Component, Fragment } from "react";
import styled, { css } from "styled-components";

const FixedFooter = styled.footer`
  background-color: #f9f8fc;
  position: fixed;
  bottom: 0;
  a {
    margin-right: 25px;
    font-family: '"Lato"';
    color: #9f9f9f;
    font-size: 12px;
    line-height: 31px;
    font-weight: normal;
    &:last-child {
      margin-right: 0;
    }
  }
`;

class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <FixedFooter>
        <a href="/">Help/FAQ</a>
        <a href="/">Contact</a>
        <a>
          Private Policy
        </a>
        <a href="/">Terms & Conditions</a>
      </FixedFooter>
    );
  }
}

export default Footer;
