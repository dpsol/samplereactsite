import React from 'react';
import Chip from "@material-ui/core/Chip";

class FilterChip extends React.Component {
    constructor(props) {
        super(props);
    }

    handleDelete = index => {
        this.props.handleDelete(index);
    }

    render() {
        if (this.props.filter.length === undefined || this.props.filter.length < 1) {
            return null;
        }
    
        this.props.filter.map((item, index) => {
            return (
              <Chip
                key={index}
                label={item}
                onDelete={() => this.handleDelete(index)}
              />
            );
        })
    }
}

export default FilterChip;