import React, { Component, Fragment } from "react";
import styled, { css } from "styled-components";

const StepDot = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #d4d3d1;
  width: 20;
  height: 20;
  border-radius: 50%;
`;

class Stepper extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Fragment>
        <div
          style={{
            display: "flex",
            widht: "100%",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          {this.props.steps.map((label, index) => {
            return (
              <div style={{ flex: 1, position: "relative" }}>
                <div
                  style={{
                    top: 7,
                    left: "calc(-50% + 20px)",
                    right: "calc(50% + 20px)",
                    position: "absolute",
                    backgroundColor: "#bdbdbd"
                  }}
                >
                  <span
                    style={{
                      borderTopStyle: "solid",
                      borderTopWidth: 2,
                      transition: "all 0.2s linear",
                      width: index <= this.props.activeStep ? "100%" : 0,
                      display: index === 0 ? "none" : "block",
                      borderColor:
                        index <= this.props.activeStep
                          ? "#000"
                          : "rgb(212, 211, 209)"
                    }}
                  />
                </div>
                <div
                  style={{
                    display: "flex",
                    margin: "0 auto 8px auto",
                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor:
                      index === this.props.activeStep ||
                      index < this.props.activeStep
                        ? "#000"
                        : "#D4D3D1",
                    width: 16,
                    height: 16,
                    borderRadius: "50%",
                    transition: "all 0.2s linear",
                    transitionDelay: "0.2s"
                  }}
                >
                  <div
                    style={{
                      backgroundColor:
                        index < this.props.activeStep ? "#000" : "#FFF",
                      borderRadius: "50%",
                      transition: "all 0.2s linear",
                      transitionDelay: "0.2s",
                      width: index === this.props.activeStep ? 6 : 12,
                      height: index === this.props.activeStep ? 6 : 12
                    }}
                  />
                </div>
                <p
                  style={{
                    textAlign: "center",
                    fontSize: 12,
                    transition: "all 0.2s linear",
                    transitionDelay: "0.2s",
                    color:
                      index === this.props.activeStep ? "#000000" : "#B0B0B0"
                  }}
                >
                  {label}
                </p>
              </div>
            );
          })}
        </div>
      </Fragment>
    );
  }
}

export default Stepper;
