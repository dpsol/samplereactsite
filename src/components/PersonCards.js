import React from 'react';
import { Avatar, Typography, Button, Paper } from '@material-ui/core';
import { Grid } from '@material-ui/core';

const PeopleList = props => {
    return (
        <Grid justify="space-between" alignItems="flex-start" container style={{paddingBottom: 15, width:'100%'}}>
          {props.people.map(person => 
            <Grid item style={{margin:20}}>
              <PersonCard
                key={person.id}
                person={person}
                showAddFriend={props.showAddFriend}
                onAddFriend={props.onAddFriend}
                onRemoveFriend={props.onRemoveFriend}
                onDeleteRequest={props.onDeleteRequest}
              />
            </Grid>
          )}
        </Grid>
    )
}

const PersonCard = (props) => {
    return (
        <Paper style={{width:140, height:180, paddingTop:20}}>
          <Grid container alignItems="center" justify="space-between" direction="column">
            <Grid item xs={12}>
              <Avatar
                alt={props.person.username}
                src={props.person.photo}
                style={{width: "90px",
                  height: "90px"}} />
            </Grid>
            <Grid item xs={12}>
              <Typography color="secondary" variant="body2" noWrap={true} style={{margin:10}} >{props.person.username}</Typography>
            </Grid>
          </Grid>
          {props.showAddFriend &&
            <Grid container alignItems="stretch" justify="space-between" direction="column">
              <Grid item xs={12}>
                <Button 
                  color="primary" 
                  variant="contained" 
                  fullWidth 
                  size="small" 
                  style={{marginBottom:0}}
                  onClick={() => props.onAddFriend(props.person)}>+ Add</Button>
              </Grid>
            </Grid>
          }
          {props.onRemoveFriend !== undefined &&
            <Grid container alignItems="stretch" justify="space-between" direction="column">
              <Grid item xs={12}>
                <Button 
                  color="primary" 
                  variant="contained" 
                  fullWidth 
                  size="small" 
                  style={{marginBottom:0}}
                  onClick={() => props.onRemoveFriend(props.person)}>x Remove</Button>
              </Grid>
            </Grid>
          }
          {props.onDeleteRequest !== undefined &&
            <Grid container alignItems="stretch" justify="space-between" direction="column">
              <Grid item xs={12}>
                <Button 
                  color="primary" 
                  variant="contained" 
                  fullWidth 
                  size="small" 
                  style={{marginBottom:0}}
                  onClick={() => props.onDeleteRequest(props.person)}>x Remove</Button>
              </Grid>
            </Grid>
          }
        </Paper>
    )
}

export default PeopleList;