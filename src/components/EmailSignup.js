import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { PrimaryButton } from "../components/Button";
import { withFirebase } from "../firebase";
import { Mixpanel } from "../utils/MixpanelWrapper";

const ERROR_CODE_ACCOUNT_EXISTS = "auth/email-already-in-use";

const ERROR_MSG_ACCOUNT_EXISTS = `
An account with this E-Mail address already exists.
Try to login with this account instead. If you think the
account is already used from one of the social logins, try
to sign-in with one of them. Afterward, associate your accounts on your personal account page.`;

const EmailSignUpPage = () => <EmailSignupForm />;

const INITIAL_STATE = {
  username: "",
  lastName: "",
  email: "",
  passwordOne: "",
  error: null
};

class EmailSignupBase extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  _signupWithEmail = event => {
    Mixpanel.track("Website - Signup.Click.");
    const { username, lastName, email, passwordOne } = this.state;

    // Useraccount = first part of email and unique
    let useraccount = email.split("@")[0];
    // Search username to avoid duplicates
    this.props.firebase
      .users()
      .where("username", "==", useraccount)
      .get()
      .then(querySnapshot => {
        useraccount += Math.random()
          .toString(36)
          .slice(-5);
      });

    this.props.firebase
      .doCreateUserWithEmailAndPassword(email, passwordOne)
      .then(authUser => {
        // Create a user in your own accessible Firebase Database too
        // Useraccount = first part of email and unique
        let useraccount = email.split("@")[0];
        this.props.firebase
          .user(authUser.user.uid)
          .set(
            {
              firstname: username,
              lastname: lastName,
              username: useraccount,
              email: email
            },
            { merge: true }
          )
          .then(() => {
            this.setState({ ...INITIAL_STATE });
            Mixpanel.track("Website - Signup.Successfull.");
            localStorage.setItem("providerid", "password");
            this.props.history.push("/app");
          })
          .catch(error => {
            Mixpanel.track("Website - Signup.Failed.");
            if (error.code === ERROR_CODE_ACCOUNT_EXISTS) {
              error.message = ERROR_MSG_ACCOUNT_EXISTS;
            }

            // TODO: Send to logger
            alert(error.message);
            this.setState({ error: error });
          });

      })
      .catch(error => {
        Mixpanel.track("Website - Signup.Failed.");
        if (error.code === ERROR_CODE_ACCOUNT_EXISTS) {
          error.message = ERROR_MSG_ACCOUNT_EXISTS;
        }
        alert(error.message);
        this.setState({ error: error });
      });

    event.preventDefault();
  };

  render() {
    return (
      <form onSubmit={this._signupWithEmail}>
        <input
          className="color-input"
          type="text"
          placeholder="First name"
          value={this.state.username}
          onChange={event => this.setState({ username: event.target.value })}
          required
        />
        <input
          className="color-input"
          type="text"
          placeholder="Last name"
          value={this.state.lastName}
          onChange={event => this.setState({ lastName: event.target.value })}
          required
        />
        <input
          className="color-input"
          type="email"
          placeholder="Email"
          value={this.state.email}
          onChange={event => this.setState({ email: event.target.value })}
          required
        />
        <input
          className="color-input"
          type="password"
          placeholder="Password"
          value={this.state.passwordOne}
          onChange={event => this.setState({ passwordOne: event.target.value })}
          required
        />
        <PrimaryButton fullWidth style={{ marginBottom: "60px" }}>
          Sign up
        </PrimaryButton>
      </form>
    );
  }
}

const EmailSignupForm = withRouter(withFirebase(EmailSignupBase));

export default EmailSignUpPage;

export { EmailSignupForm };
