import styled, { css } from "styled-components";

export const Button = styled.button`
  background: transparent;
  cursor: pointer;
  font-size: 16px;
  line-height: 21px;
  font-family: "Lato";
  transition: all 0.1s linear;
  padding: 0;

  ${props =>
    props.fullWidth &&
    css`
      width: 100%;
      display: block;
    `};
`;

export const DefaultButton = styled(Button)`
  background: #f9f9f9;
  font-size: 16px;
  color: #000;
  font-weight: 300;
  &:hover {
    background: #f0f0f0;
  }
`;

export const BorderButton = styled(Button)`
  border: solid 1px #fc6e5b;
  color: #fc6e5b;
  &:hover {
    opacity: 0.8;
  }
`;

export const PrimaryButton = styled(Button)`
  background: #fc6e5b;
  color: #fff;
  &:hover {
    opacity: 0.8;
  }
  ${props =>
    props.disabled &&
    css`
      cursor: auto;
      color: #000;
      font-weight: 300;
      background: #eaeaea;
    `};
`;

export const FacebookButton = styled(Button)`
  background: #4e70c4;
  color: #fff;
  &:hover {
    opacity: 0.8;
  }
`;

export const GoogleButton = styled(Button)`
  background: #ff5a5a;
  color: #fff;

  &:hover {
    opacity: 0.8;
  }
`;
