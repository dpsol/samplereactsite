import React from 'react'
import { storage } from '../firebase/firebase';

class FileUpload extends React.Component {
    constructor () {
        super()
        this.state = {
            uploadValue: 0,
            result: ''
        }
    }

    handleOnChange (event) {
        const file = event.target.files[0];
        const storageRef = storage.ref(`UserVideos/${file.name}`);
        const task = storageRef.put(file);
    
        task.on('state_changed', (snapshot) => {
          let percentage = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          this.setState({
            uploadValue: percentage
          });
        }, (error) => {
          console.error(error.message);
          this.setState({
            result: 'Failed!'
          });
        }, () => {
          // Upload complete
          this.setState({
            result: 'Success!'
          });
        })
      }

      render () {
        return (
          <div>
            <progress value={this.state.uploadValue} max='100'>
              {this.state.uploadValue} %
            </progress>
            <br />
            <label className="btn btn-default btn-file">
                Browse Video file <input type="file" style={{display: 'none'}} onChange={this.handleOnChange.bind(this)} />
            </label>
            <br />
            <label>{this.state.result}</label>
          </div>
        )
      }
  }

  export default FileUpload;
