import React from "react";
import { Link } from "react-router-dom";
import { Grid, Hidden } from "@material-ui/core";
import styled, { css } from "styled-components";
import { List, ListItem, ListItemIcon, ListItemText } from "@material-ui/core";
import User from "../assets/user.png";
import UserWhite from "../assets/user-white.png";
import Friends from "../assets/friends.png";
import FriendsWhite from "../assets/friends-white.png";
import Heart from "../assets/heart.png";
import HeartWhite from "../assets/heart-white.png";
import Gift from "../assets/gift.png";
import GiftWhite from "../assets/gift-white.png";
import Favorites from "../assets/favorites.png";
import FavoritesWhite from "../assets/favorites-white.png";
import Settings from "../assets/settings.png";
import SettingsWhite from "../assets/settings-white.png";
import Help from "../assets/help.png";
import HelpWhite from "../assets/help-white.png";

const styles = theme => ({
  root: {
    paddingTop: 30,
    paddingLeft: 30,
    paddingRight: 30
  },
  logoutIcon: {
    marginRight: theme.spacing.unit
  },
  active: {
    backgroundColor: theme.palette.action.selected
  },
  header: {
    padding: 40
  },
  questions: {
    color: "red"
  }
});

export const SideMenuButton = styled.a`
  display: flex;
  align-items: center;
  line-height: 50px;
  width: 200px;
  margin: 0 auto;
`;

export const SideMenuLink = styled(Link)`
  display: block;
  background: ${props =>
    props.opSelected === props.option ? "#FC6E5B" : "#FFFFFF"};
  &:hover {
    background: ${props =>
      props.opSelected === props.option
        ? "#FC6E5B"
        : "rgba(252, 110, 91, 0.1)"};
  }
`;

const LeftMenu = props => (
  <Grid
    item
    xs={3}
    alignItems="center"
    justify="center"
    style={{
      backgroundColor: "#FFFFFF",
      boxShadow: "2px -2px 11px rgba(0, 0, 0, 0.03)"
    }}
  >
    <List>
      <SideMenuLink
        to={`/app/users/123`}
        opSelected={props.opSelected}
        option={1}
      >
        <SideMenuButton key={"Profile"}>
          <img
            src={props.opSelected == 1 ? UserWhite : User}
            alt="Account"
            style={{ width: 18, marginRight: 15 }}
          />
          <p
            style={{
              fontFamily: "Lato",
              fontSize: 14,
              fontWeight: 300,
              color: props.opSelected == 1 ? "#FFFFFF" : "#000000"
            }}
          >
            Account Profile
          </p>
        </SideMenuButton>
      </SideMenuLink>
      <SideMenuLink
        to={"/app/favorites"}
        opSelected={props.opSelected}
        option={6}
      >
        <SideMenuButton key={"Favorites"}>
          <img
            src={props.opSelected == 6 ? HeartWhite : Heart}
            alt="Favorites"
            style={{ width: 18, marginRight: 15 }}
          />
          <p
            style={{
              fontFamily: "Lato",
              fontSize: 14,
              fontWeight: 300,
              color: props.opSelected == 6 ? "#FFFFFF" : "#000000"
            }}
          >
            Favorites
          </p>
        </SideMenuButton>
      </SideMenuLink>
      <SideMenuLink
        to={"/app/wishlist"}
        opSelected={props.opSelected}
        option={5}
      >
        <SideMenuButton key={"Wishlist"}>
          <img
            src={props.opSelected == 5 ? FavoritesWhite : Favorites}
            alt="Wishlist"
            style={{ width: 18, marginRight: 15 }}
          />
          <p
            style={{
              fontFamily: "Lato",
              fontSize: 14,
              fontWeight: 300,
              color: props.opSelected == 5 ? "#FFFFFF" : "#000000"
            }}
          >
            Wishlist
          </p>
        </SideMenuButton>
      </SideMenuLink>
      <SideMenuLink
        to={"/app/friendscontacts"}
        opSelected={props.opSelected}
        option={7}
      >
        <SideMenuButton key={"FriendsAndContacts"}>
          <img
            src={props.opSelected == 7 ? FriendsWhite : Friends}
            alt="Wishlist"
            style={{ width: 18, marginRight: 15 }}
          />
          <p
            style={{
              fontFamily: "Lato",
              fontSize: 14,
              fontWeight: 300,
              color: props.opSelected == 7 ? "#FFFFFF" : "#000000"
            }}
          >
            Friends & Contacts
          </p>
        </SideMenuButton>
      </SideMenuLink>
      <SideMenuLink
        to={"/app/giftssent/"}
        opSelected={props.opSelected}
        option={2}
      >
        <SideMenuButton key={"Gifts"}>
          <img
            src={props.opSelected == 2 ? GiftWhite : Gift}
            alt="Gifts"
            style={{ width: 18, marginRight: 15 }}
          />
          <p
            style={{
              fontFamily: "Lato",
              fontSize: 14,
              fontWeight: 300,
              color: props.opSelected == 2 ? "#FFFFFF" : "#000000"
            }}
          >
            Gifts Sent & Received
          </p>
        </SideMenuButton>
      </SideMenuLink>
      <SideMenuLink
        to={"/app/users/settings"}
        opSelected={props.opSelected}
        option={3}
      >
        <SideMenuButton key={"Settings"}>
          <img
            src={props.opSelected == 3 ? SettingsWhite : Settings}
            alt="Settings"
            style={{ width: 18, marginRight: 15 }}
          />
          <p
            style={{
              fontFamily: "Lato",
              fontSize: 14,

              fontWeight: 300,
              color: props.opSelected == 3 ? "#FFFFFF" : "#000000"
            }}
          >
            Settings
          </p>
        </SideMenuButton>
      </SideMenuLink>
      <SideMenuLink to={"/app/help/"} opSelected={props.opSelected} option={4}>
        <SideMenuButton key={"Help"}>
          <img
            src={props.opSelected == 4 ? HelpWhite : Help}
            alt="Help"
            style={{ width: 18, marginRight: 15 }}
          />
          <p
            style={{
              fontFamily: "Lato",
              fontSize: 14,
              fontWeight: 300,
              color: props.opSelected == 4 ? "#FFFFFF" : "#000000"
            }}
          >
            Help
          </p>
        </SideMenuButton>
      </SideMenuLink>
    </List>
  </Grid>
);
export default LeftMenu;
