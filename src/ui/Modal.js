import React from 'react';
import PropTypes from 'prop-types';

class Modal extends React.Component {
  render() {
    // Render nothing if the "show" prop is false
    if(!this.props.show) {
      return null;
    }

    // The gray background
    const backdropStyle = {
      position: 'fixed',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      backgroundColor: 'rgba(0,0,0,0.3)',
      padding: 50
    };

    // The modal "window"
    const modalStyle = {
      backgroundColor: '#fff',
      borderRadius: 5,
      maxWidth: 300,
      minHeight: 50,
      margin: '0 auto',
      padding: 30,
      maxHeight: "80vh",
      overflowY: "auto"
    };

    return (
      <div style={backdropStyle}>
        <div style={modalStyle}>
          <button className="btn btn-checkout-next" onClick={this.props.onClose}>
              Close
          </button>
          <hr />
          {this.props.children}
          <hr />
          <div>
            <button className="btn btn-checkout-next" onClick={this.props.onClose}>
              Close
            </button>
          </div>
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  onClose: PropTypes.func.isRequired,
  show: PropTypes.bool,
  children: PropTypes.node
};

export default Modal;