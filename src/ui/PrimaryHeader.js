import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import styled, { css } from "styled-components";
import { withStyles } from "@material-ui/core/styles";
import { withFirebase } from "../firebase";
import {
  Typography,
  Hidden,
  IconButton,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Drawer from "@material-ui/core/Drawer";
import Logo from "../assets/logo.png";
import { Mixpanel } from "../utils/MixpanelWrapper";
import Person from "../assets/sidebar/person.png";
import Friends from "../assets/sidebar/friends.png";
import Gift from "../assets/gift.png";
import Favorite from "../assets/sidebar/favorites.png";
import Wishlist from "../assets/sidebar/wishlist.png";
import Settings from "../assets/sidebar/settings.png";
import Help from "../assets/sidebar/help.png";
import Logout from "../assets/sidebar/logout.png";

const styles = theme => ({
  headerLink: {
    fontFamily: "Lato",
    color: "#5A6175",
    fontSize: 16,
    padding: 15
  },
  root: {
    flexGrow: 1
  },
  grow: {
    flexGrow: 1
  },
  logoutIcon: {
    marginRight: theme.spacing.unit
  },
  avatar: {
    marginRight: 15,
    width: 60,
    height: 60,
    borderRadius: "50%",
    opacity: 0.5
  },
  avatarphotoURL: {
    marginRight: 15,
    width: 60,
    height: 60,
    borderRadius: "50%",
    opacity: 1
  },
  list: {
    width: "100%",
    maxWidth: 405,
    top: 76
  },
  subHeader: {
    display: "flex",
    alignItems: "center",
    padding: 30,
    marginBottom: 15,
    backgroundColor: "#fc6e5b"
  },
  nameSubHeader: {
    color: "white",
    fontSize: 20
  },
  emailSubHeader: {
    color: "white",
    fontSize: 14
  }
});

const Header = styled.header`
  background: #ffffff;
  font-family: Lato;
  position: sticky;
  top: 0;
  left: 0;
  width: 100%;
  z-index: 2;
  height: 76px;
  padding-left: 135px;
  padding-right: 135px;
  display: flex;
  justify-content: center;
  align-items: center;
  box-shadow: 0px 2px 11px rgba(0, 0, 0, 0.03);
  @media (min-width: 768px) and (max-width: 1024px) {
    padding-left: 20px;
    padding-right: 20px;
  }
`;

const DrawerMenu = styled.div`
  width: "405px";
`;

const Button = styled.button`
  color: #5a6175;
  font-family: "Lato";
  font-size: "16px";
  line-height: "19px";
`;

class PrimaryHeaderBase extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      openFriends: false
    };
  }

  handleLogout = () => {
    this.props.firebase.doSignOut().then(() => {
      Mixpanel.track("Website - Logout.");
    });
  };

  toggleDrawer = open => () => {
    this.setState({
      open: open
    });
  };

  toggleDrawerGifts = open => () => {
    this.setState({
      open: open
    });
  };

  handleClose = event => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }

    this.setState({ open: false });
  };

  handleToggleFriends = () => {
    this.setState(state => ({ openFriends: !state.openFriends }));
  };

  handleCloseFriends = event => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }

    this.setState({ openFriends: false });
  };

  render() {
    const { classes } = this.props;
    const { open, openFriends } = this.state;

    const headerList = (
      <div className={classes.subHeader}>
        <img
          src={
            this.props.user.photoURL != null
              ? this.props.user.photoURL
              : "/images/avatar.png"
          }
          alt="User"
          className={
            this.props.user.photoURL != null
              ? classes.avatarphotoURL
              : classes.avatar
          }
        />
        <ListItemText
          primary={
            <Typography className={classes.nameSubHeader}>
              {this.props.user.firstname} {this.props.user.lastname}
            </Typography>
          }
          secondary={
            <Typography className={classes.emailSubHeader}>
              {this.props.user.email}
            </Typography>
          }
        />
      </div>
    );

    const sideList = (
      <div className={classes.list}>
        <List subheader={headerList}>
          <Link
            to={`/app/users/${this.props.user.id}`}
            style={{ textDecoration: "none" }}
          >
            <ListItem
              button
              key={"Profile"}
              style={{ padding: "15px 30px 15px 30px" }}
            >
              <img src={Person} alt="User" style={{ width: 18 }} />
              <ListItemText secondary={"Account Profile"} />
            </ListItem>
          </Link>
          <Link to={"/app/favorites"} style={{ textDecoration: "none" }}>
            <ListItem
              button
              key={"Favorites"}
              style={{ padding: "15px 30px 15px 30px" }}
            >
              <img src={Favorite} alt="Favorite" style={{ width: 18 }} />
              <ListItemText secondary={"Favorites"} />
            </ListItem>
          </Link>
          <Link to={"/app/wishlist"} style={{ textDecoration: "none" }}>
            <ListItem
              button
              key={"Wishlist"}
              style={{ padding: "15px 30px 15px 30px" }}
            >
              <img src={Wishlist} alt="Wishlist" style={{ width: 18 }} />
              <ListItemText secondary={"Wishlist"} />
            </ListItem>
          </Link>
          <Link to={"/app/friendscontacts"} style={{ textDecoration: "none" }}>
            <ListItem
              button
              key={"friends"}
              style={{ padding: "15px 30px 15px 30px" }}
            >
              <img src={Friends} alt="Friends" style={{ width: 18 }} />
              <ListItemText secondary={"Friends & Contacts"} />
            </ListItem>
          </Link>
          <Link to={"/app/giftssent/"} style={{ textDecoration: "none" }}>
            <ListItem
              button
              key={"GiftsSent"}
              style={{ padding: "15px 30px 15px 30px" }}
            >
              <img src={Gift} alt="Gifts" style={{ width: 18 }} />
              <ListItemText secondary={"Your Gifts"} />
            </ListItem>
          </Link>
          <Link to={"/app/users/settings"} style={{ textDecoration: "none" }}>
            <ListItem
              button
              key={"UserSettings"}
              style={{ padding: "15px 30px 15px 30px" }}
            >
              <img src={Settings} alt="Settings" style={{ width: 18 }} />
              <ListItemText secondary={"Settings"} />
            </ListItem>
          </Link>
          <Link to={"/app/help/"} style={{ textDecoration: "none" }}>
            <ListItem
              button
              key={"Help"}
              style={{ padding: "15px 30px 15px 30px" }}
            >
              <img src={Help} alt="Help" style={{ width: 18 }} />
              <ListItemText secondary={"Help"} />
            </ListItem>
          </Link>
          <Link to={"/"} style={{ textDecoration: "none" }}>
            <ListItem
              button
              key={"Logout"}
              onClick={this.handleLogout}
              style={{ padding: "15px 30px 15px 30px" }}
            >
              <img src={Logout} alt="Logout" style={{ width: 18 }} />
              <ListItemText secondary={"Logout"} />
            </ListItem>
          </Link>
        </List>
      </div>
    );

    return (
      <Header>
        <div className={classes.grow}>
          <Link to="/app/home/">
            <img src={Logo} alt="" style={{ width: 140 }} />
          </Link>
        </div>
        <Hidden only="xs">
          <Link to={"/app/home/"} className={classes.headerLink}>
            Store
          </Link>
          <Link to={"/app/giftssent/"} className={classes.headerLink}>
            Your Gifts
          </Link>
          <button
            onClick={this.toggleDrawer(true)}
            style={{ display: "flex", alignItems: "center" }}
          >
            <img
              src={
                this.props.user.photoURL != null
                  ? this.props.user.photoURL
                  : "/images/avatar.png"
              }
              alt="User"
              style={{
                width: 40,
                height: 40,
                borderRadius: 20,
                marginRight: 15
              }}
            />
            <p style={{ fontFamily: "Lato", fontSize: 16 }}>
              {this.props.user.firstname} {this.props.user.lastname}
            </p>
          </button>
        </Hidden>
        <Hidden smUp>
          <IconButton
            onClick={this.toggleDrawer(true)}
            className={classes.menuButton}
            color="inherit"
            aria-label="Menu"
          >
            <MenuIcon />
          </IconButton>
        </Hidden>
        <Drawer
          anchor="right"
          open={this.state.open}
          onClose={this.toggleDrawer(false)}
        >
          <DrawerMenu
            tabIndex={0}
            role="button"
            onClick={this.toggleDrawer(false)}
            onKeyDown={this.toggleDrawer(false)}
          >
            {sideList}
          </DrawerMenu>
        </Drawer>
      </Header>
    );
  }
}

PrimaryHeaderBase.propTypes = {
  classes: PropTypes.object.isRequired
};

const PrimaryHeader = withFirebase(withStyles(styles)(PrimaryHeaderBase));

export default PrimaryHeader;
