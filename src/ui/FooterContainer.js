import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Typography, Button, Grid } from '@material-ui/core';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

// Theme's styles
const styles = {
  row: {
    display: 'flex',
    justifyContent: 'center',
  },
  demo: {
  },
  avatar: {
    margin: 10,
  },
  bigAvatar: {
    margin: 10,
    width: 60,
    height: 60,
    borderRadious: 0
  },
};

// Theme's constants
const theme = createMuiTheme({
  palette: {
    type: 'light',
    primary: {
      light: '#fc6e5b',
      main: '#fc6e5b',
      // dark: auto
      // contrastText: auto
    },
    secondary: {
      light: '#a8afb6',
      main: '#9f9f9f',
      // dark will be calc
      //contrastText: '#ffcc00',
    },
    // error:
  },
  typography: {
    useNextVariants: true,
  },
  spacing: {
    unit: 8,
  },
  overrides: {
    MuiButton: {
      containedPrimary: {
        color: 'white',
      },
      textSecondary: {
        fontWeight: 400,
        fontSize: 14,
        textTransform: 'none'
      },
    }
  }
})

class Footer extends React.Component {
  state = {
    spacing: '16',
    alignItems: 'center',
  };
    
  render() {
    const { classes } = this.props;
    const { alignItems } = this.state;
    const currentYear = new Date().getFullYear()
    return (
      <MuiThemeProvider theme={theme}>
        <Grid container className={classes.root}>
          <Grid item xs={12}>
            <Grid container className={classes.demo} justify="center" alignItems={alignItems}>
              <Grid item>
                <Button href="https://app.termly.io/document/privacy-policy/58dac287-3e42-486e-917b-4e5b803434b8" target="_blank" 
                  color="secondary">Private Policy</Button>
              </Grid>
              <Grid item>
                <Button href="https://app.termly.io/document/terms-of-use-for-online-marketplace/858e1ce2-fb16-40db-9029-3f319f7ed4b5" target="_blank"
                  color="secondary">Terms &amp; Conditions</Button>
              </Grid>
              <Grid item>
                <Typography variant="body2" color="secondary">&copy; {currentYear} .</Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </MuiThemeProvider>
    );
  }
}

Footer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Footer);