import React, { Component } from 'react';
//import '../styles/barStepsCheckout.css';

const BarStepsCheckout = (props) => (
     <div className="bar">
        <span className="bar-checkout">CHECKOUT</span>
        <div className="bar-steps">
            <div className="step">
                <span className={'circle circle-contact-'+ props.step}></span>
                <hr className={'hr-step hr-step-contact-'+ props.step}/>
                <span className="text-step">Recipient</span>
            </div>
            <div className="step">
                <span className={'circle circle-personalize-'+ props.step}></span>
                <hr className={'hr-step hr-step-personalize-'+ props.step}/>
                <span className="text-step">Personalize</span>
            </div>
            <div className="step">
                <span className={'circle circle-payment-'+ props.step}></span>
                <hr className={'hr-step hr-step-payment-'+ props.step}/>
                <span className="text-step">Payment</span>
            </div>
            <div className="summary-step">
                <span className="summary-circle"></span>
                <span className="text-step">Summary</span>
            </div>
        </div>
     </div>
);

export default BarStepsCheckout;