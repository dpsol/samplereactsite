import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import {
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction
} from "@material-ui/core";
// import { NONAME } from "dns";

const styles = theme => ({
  toolbar: theme.mixins.toolbar
});

const FilterOptions = props => {
  const { classes, arrayOptions } = props;
  return (
    <div>
      {arrayOptions.map(({ id, title, count, selected }) => (
        <div
          style={{
            position: "relative",
            display: "flex",
            alignItems: "center"
          }}
        >
          <button
            key={id}
            onClick={props.onChange}
            selected={selected}
            style={{
              listStyle: "none",
              display: "block",
              width: "100%",
              height: 50,
              paddingLeft: 16,
              paddingRight: 16,
              textAlign: "left",
              fontSize: 14,
              fontFamily: "Lato"
            }}
          >
            {title}
          </button>
          <div
            style={{
              position: "absolute",
              bottom: 0,
              width: "calc(100% - 32px)",
              height: 1,
              backgroundColor: "rgba(90,97,117,0.1)",
              margin: "0 16px"
            }}
          />
        </div>
      ))}
    </div>
  );
};

FilterOptions.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(FilterOptions);
