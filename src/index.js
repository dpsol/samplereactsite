import React from "react";
import { render } from "react-dom";
import App from "./App";
import Firebase, { FirebaseContext } from "./firebase";

import * as serviceWorker from "./serviceWorker";

import { Mixpanel } from "./utils/MixpanelWrapper";

Mixpanel.track("Website - Landing.");

render(
  <FirebaseContext.Provider value={new Firebase()}>
    <App />
  </FirebaseContext.Provider>,
  document.getElementById("root")
);

if (module.hot) {
  module.hot.accept();
}
serviceWorker.unregister();
