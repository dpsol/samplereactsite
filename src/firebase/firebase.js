import app from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";

const config = {
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_DATABASE_URL,
  projectId: process.env.REACT_APP_PROJECT_ID,
  storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID
};

class Firebase {
    constructor() {
      app.initializeApp(config);

      this.auth = app.auth();
      this.db = app.firestore();
      this.storage = app.storage();

      this.emailAuthProvider = app.auth.EmailAuthProvider;
      this.googleProvider = new app.auth.GoogleAuthProvider();
      this.facebookProvider = new app.auth.FacebookAuthProvider();
    }

    // *** Auth API ***
    doCreateUserWithEmailAndPassword = (email, password) =>
      this.auth.createUserWithEmailAndPassword(email, password);

    doSignInWithEmailAndPassword = (email, password) =>
        this.auth.signInWithEmailAndPassword(email, password);

    doSignInWithGoogle = () =>
        this.auth.signInWithPopup(this.googleProvider);

    doSignInWithFacebook = () =>
        this.auth.signInWithPopup(this.facebookProvider);

    doSignOut = () => this.auth.signOut();

    doPasswordReset = email => this.auth.sendPasswordResetEmail(email);
    
    doPasswordUpdate = password =>
        this.auth.currentUser.updatePassword(password);
    
    doSendEmailVerification = () =>
        this.auth.currentUser.sendEmailVerification({
            url: process.env.REACT_APP_CONFIRMATION_EMAIL_REDIRECT,
        });

    onAuthUserListener = (next, fallback) =>
        this.auth.onAuthStateChanged(authUser => {
          if (authUser) {
            this.user(authUser.uid)
              .get()
              .then(snapshot => {
                const dbUser = snapshot.data();
                // merge auth and db user
                authUser = {
                    uid: authUser.uid,
                    email: authUser.email,
                    emailVerified: authUser.emailVerified, 
                    providerData: authUser.providerData,
                    ...dbUser,
                };
                next(authUser);
              });
          } else {
            fallback();
          }
        });

    // *** Firestore Users API ***
    user = uid => this.db.collection("users").doc(uid);
    users = () => this.db.collection("users");
    
    // *** Firestore Gift API ***
    gift = giftid => this.db.collection("gift").doc(giftid);
    gifts = () => this.db.collection("gift");

    // *** Firestore Variant API ***
    variant = vid => this.db.collection("Variant").doc(vid);
    variants = () => this.db.collection("Variant");

    // *** Firestore params API ***
    param = pid => this.db.collection("params").doc(pid);
    params = () => this.db.collection("params");

    // *** Firestore emailTemplates API ***
    template = tid => this.db.collection("emailTemplates").doc(tid);
    templates = () => this.db.collection("emailTemplates");

    // *** Firestore locations API ***
    location = lid => this.db.collection("locations").doc(lid);
    locations = () => this.db.collection("locations");

    // *** Firestore locations-variants API ***
    locationvariant = lvid => this.db.collection("LocationVariants").doc(lvid);
    locationvariants = () => this.db.collection("LocationVariants");

    // *** Firestore cities API ***
    city = cid => this.db.collection("Cities").doc(cid);
    cities = () => this.db.collection("Cities");

    // *** Firestore giftEmail API ***
    giftEmail = () => this.db.collection("giftEmail");

    // *** Storage API ***
    storageRef = file => this.storage.ref(file);
}

export default Firebase;
